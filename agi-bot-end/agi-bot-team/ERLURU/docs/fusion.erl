api.erl [{deps, [
    {cowboy, "2.9.0"},
    {jsx, "3.2.0"}
]}.

- api
  - api.erl
  - api.hrl
- pessoas_handler
  - pessoas_handler.erl
- recursos_handler
  - recursos_handler.erl

%%% api.erl %%%

-module(api).
-export([start/0, allocate_resources/0]).

%% Resource allocation variables
-define(TOTAL_CPUS, 1.5).
-define(TOTAL_MEMORY, 3000).

%% Resource usage variables
-define(CPU_USAGE_THRESHOLD, 0.7).
-define(MEMORY_USAGE_THRESHOLD, 0.8).

%% Resource allocation state
-record(resources, {
    allocated_cpus = 0.0,
    allocated_memory = 0.0
}).

%% Start the server
start() ->
    Dispatch = cowboy_router:compile([
        {'_', [
            {"/pessoas", cowboy_rest, [
                {"/", handle_create, []}
            ]}
        ]}
    ]),

    %% Start the HTTP server on port 8080
    {ok, _} = cowboy:start_clear(http, [
        {port, 8080}
    ]),

    %% Set the server routes
    cowboy_router:unset_env(http, Dispatch),

    %% Start resource allocation
    allocate_resources().

%% Function to handle person creation
handle_create(Req, State) ->
    {ok, Body, Req2} = cowboy_req:body(Req),
    case jsx:decode(Body) of
        {ok, Data} ->
            %% Here you will have the decoded object from the JSON request
            %% Now you can use the ID to save the person in the database, for example
            io:format("Create person: ~p~n", [Data]),
            {ok, Req2, State};
        {error, Reason} ->
            io:format("JSON decoding error: ~p~n", [Reason]),
            {bad_request, Req2, State}
    end.

%% Resource allocation
allocate_resources() ->
    Monitor = spawn_link(fun monitor/0),
    loop(Monitor).

%% Monitor CPU and memory usage
monitor() ->
    {ok, Pid} = os:cmd("top -n 1 -b | awk '/^%Cpu/{print $2}'"),
    CpuUsage = list_to_float(string:strip(Pid)),
    {ok, Pid2} = os:cmd("free | awk '/Mem/{print $3/$2}'"),
    MemoryUsage = list_to_float(string:strip(Pid2)),

    %% Update allocated resources
    NewResources = update_resources(CpuUsage, MemoryUsage),

    %% Check if automatic scaling is necessary
    case NewResources of
        true -> spawn_link(fun auto_scale/0);
        _ -> ok
    end,

    %% Sleep for a certain interval before monitoring again
    timer:sleep(5000),
    monitor().

%% Update allocated resources
update_resources(CpuUsage, MemoryUsage) ->
    TotalCpus = ?TOTAL_CPUS,
    TotalMemory = ?TOTAL_MEMORY,
    NewAllocatedCpus = case CpuUsage >= ?CPU_USAGE_THRESHOLD of
        true -> 0.0;
        false -> TotalCpus
    end,
    NewAllocatedMemory = case MemoryUsage >= ?MEMORY_USAGE_THRESHOLD of
        true -> 0.0;
        false -> TotalMemory
    end,
    NewResources = #resources{
        allocated_cpus = NewAllocatedCpus,
        allocated_memory = NewAllocatedMemory
    },
    io:format("Updated resources: ~p~n", [NewResources]),
    NewResources.

%% Automatic scaling function
auto_scale() ->
    %% Automatic scaling logic here
    io:format("Automatic scaling executed~n").



%%% recursos_handler.erl %%%

-module(recursos_handler).
-export([allocate_resources/0, monitor/0]).

-include_lib("cowboy/include/cowboy.hrl").

%% Function to allocate resources
allocate_resources() ->
    %% Monitor CPU and memory usage
    monitor(),
    %% Check if automatic scaling is necessary
    %% and allocate the required resources
    ...
    ok.

%% Monitor CPU and memory usage
monitor() ->
    {ok, Pid} = os:cmd("top -n 1 -b | awk '/^%Cpu/{print $2}'"),
    CpuUsage = list_to_float(string:strip(Pid)),
    {ok, Pid2} = os:cmd("free | awk '/Mem/{print $3/$2}'"),
    MemoryUsage = list_to_float(string:strip(Pid2)),

    %% Update allocated resources
    NewResources = update_resources(CpuUsage, MemoryUsage),

    %% Check if automatic scaling is necessary
    %% and allocate the required resources
    ...
    ok.
]

#---------------------------------#

pessoas_handler.erl [-module(pessoas_handler).
-export([handle_create/2, handle_get/2, handle_update/2, handle_delete/2]).

-include_lib("cowboy/include/cowboy.hrl").
-include_lib("jsx/include/jsx.hrl").

%% Função para lidar com a criação de uma pessoa
handle_create(Req, State) ->
    {ok, Body, Req2} = cowboy_req:body(Req),
    case jsx:decode(Body) of
        {ok, Data} ->
            %% Aqui você terá o objeto decodificado da requisição JSON
            %% Agora você pode usar o ID para salvar a pessoa no banco de dados, por exemplo
            io:format("Criar pessoa: ~p~n", [Data]),
            {ok, Req2, State};
        {error, Reason} ->
            io:format("Erro na decodificação JSON: ~p~n", [Reason]),
            {bad_request, Req2, State}
    end.

%% Função para lidar com a obtenção de uma pessoa
handle_get(Req, State) ->
    %% Aqui você pode obter a pessoa do banco de dados usando o ID na requisição
    %% e retornar a resposta com os dados da pessoa encontrada
    %% Por enquanto, vamos apenas retornar uma resposta vazia
    {ok, cowboy_req:reply(200, #{}, <<"">>), Req, State}.

%% Função para lidar com a atualização de uma pessoa
handle_update(Req, State) ->
    {ok, Body, Req2} = cowboy_req:body(Req),
    case jsx:decode(Body) of
        {ok, Data} ->
            %% Aqui você terá o objeto decodificado da requisição JSON
            %% Agora você pode usar o ID para atualizar os dados da pessoa no banco de dados, por exemplo
            io:format("Atualizar pessoa: ~p~n", [Data]),
            {ok, Req2, State};
        {error, Reason} ->
            io:format("Erro na decodificação JSON: ~p~n", [Reason]),
            {bad_request, Req2, State}
    end.

%% Função para lidar com a exclusão de uma pessoa
handle_delete(Req, State) ->
    %% Aqui você pode usar o ID na requisição para excluir a pessoa do banco de dados, por exemplo
    %% Por enquanto, vamos apenas retornar uma resposta vazia
    {ok, cowboy_req:reply(204, #{}, <<"">>), Req, State}.
]

#---------------------------------#

recursos_handler.erl[-module(recursos_handler).
-export([allocate_resources/0, monitor/0]).

-include_lib("cowboy/include/cowboy.hrl").

%% Função para alocar recursos
allocate_resources() ->
    %% Monitora o uso de CPU e memória
    monitor(),
    %% Verifica se é necessário fazer o escalonamento automático
    %% e aloca os recursos necessários
    auto_scale(),
    ok.

%% Monitora o uso deCPU e memória
monitor() ->
    erlang:monitor(process, {global, cpu_monitor}),
    erlang:monitor(process, {global, memory_monitor}),
    spawn_link(fun() -> cpu_monitor() end),
    spawn_link(fun() -> memory_monitor() end).

%% Monitora o uso de CPU
cpu_monitor() ->
    {ok, CpuRef} = erlang:open_port({fd, 1, 2}, [{line, 1000}]),
    loop(CpuRef, cpu).

%% Monitora o uso de memória
memory_monitor() ->
    {ok, MemoryRef} = erlang:open_port({fd, 1, 2}, [{line, 1000}]),
    loop(MemoryRef, memory).

%% Loop de monitoramento
loop(Port, Type) ->
    receive
        {Port, {data, Data}} ->
            DataTuple = list_to_tuple(Data),
            case Type of
                cpu ->
                    {_, _, _, _, _, _, _, _, _, Cpu} = DataTuple,
                    %% Verifica se é necessário fazer o escalonamento automático
                    auto_scale(Cpu);
                memory ->
                    {_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, Memory} = DataTuple,
                    %% Verifica se é necessário fazer o escalonamento automático
                    auto_scale(Memory)
            end,
            loop(Port, Type);
        {Port, closed} ->
            ok
    end.

%% Verifica se é necessário fazer o escalonamento automático
auto_scale(Cpu) ->
    case Cpu of
        70 -> %% Executa ações para escalonamento vertical
            io:format("Escalonamento vertical necessário~n");
        95 -> %% Executa ações para escalonamento horizontal
            io:format("Escalonamento horizontal necessário~n");
        _ ->
            ok
    end.]