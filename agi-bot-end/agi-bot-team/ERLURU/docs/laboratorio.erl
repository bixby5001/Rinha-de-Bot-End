certifique que app inicie em 3 regiões cada região com duas api

endpoints [Região 1:
```
curl -X GET https://8081-cs-639514086775-default.cs-us-east1-pkhd.cloudshell.dev/pessoas
curl -X POST https://8081-cs-639514086775-default.cs-us-east1-pkhd.cloudshell.dev/pessoas -H "Content-Type: application/json" -d '{"nome": "João", "idade": 25}'
curl -X GET https://8081-cs-639514086775-default.cs-us-east1-pkhd.cloudshell.dev/pessoas/1
curl -X PUT https://8081-cs-639514086775-default.cs-us-east1-pkhd.cloudshell.dev/pessoas/1 -H "Content-Type: application/json" -d '{"nome": "João Silva", "idade": 26}'
curl -X DELETE https://8081-cs-639514086775-default.cs-us-east1-pkhd.cloudshell.dev/pessoas/1
```

Região 2:
```
curl -X GET https://3000-cs-b1fa01a8-77d1-4331-8e10-a1c0717d189d.cs-us-east1-pkhd.cloudshell.dev/pessoas
curl -X POST https://3000-cs-b1fa01a8-77d1-4331-8e10-a1c0717d189d.cs-us-east1-pkhd.cloudshell.dev/pessoas -H "Content-Type: application/json" -d '{"nome": "Maria", "idade": 30}'
curl -X GET https://3000-cs-b1fa01a8-77d1-4331-8e10-a1c0717d189d.cs-us-east1-pkhd.cloudshell.dev/pessoas/2
curl -X PUT https://3000-cs-b1fa01a8-77d1-4331-8e10-a1c0717d189d.cs-us-east1-pkhd.cloudshell.dev/pessoas/2 -H "Content-Type: application/json" -d '{"nome": "Maria Souza", "idade": 31}'
curl -X DELETE https://3000-cs-b1fa01a8-77d1-4331-8e10-a1c0717d189d.cs-us-east1-pkhd.cloudshell.dev/pessoas/2
```

Região 3:
```
curl -X GET https://8080-cs-1002024752404-default.cs-us-east1-pkhd.cloudshell.dev/pessoas
curl -X POST https://8080-cs-1002024752404-default.cs-us-east1-pkhd.cloudshell.dev/pessoas -H "Content-Type: application/json" -d '{"nome": "Pedro", "idade": 35}'
curl -X GET https://8080-cs-1002024752404-default.cs-us-east1-pkhd.cloudshell.dev/pessoas/3
curl -X PUT https://8080-cs-1002024752404-default.cs-us-east1-pkhd.cloudshell.dev/pessoas/3 -H "Content-Type: application/json" -d '{"nome": "Pedro Santos", "idade": 36}'
curl -X DELETE https://8080-cs-1002024752404-default.cs-us-east1-pkhd.cloudshell.dev/pessoas/3
```
]


api [%%% api.erl %%%

-module(api).
-export([start/0, allocate_resources/0]).

%% Resource allocation variables
-define(TOTAL_CPUS, 1.5).
-define(TOTAL_MEMORY, 3000).

%% Resource usage variables
-define(CPU_USAGE_THRESHOLD, 0.7).
-define(MEMORY_USAGE_THRESHOLD, 0.8).

%% Resource allocation state
-record(resources, {
    allocated_cpus = 0.0,
    allocated_memory = 0.0
}).

%% Inicia o servidor
start() ->
    Dispatch = cowboy_router:compile([
        {'_', [
            %% Endpoint para criar uma pessoa
            {"/pessoas", cowboy_rest, [
                {"/", handle_create, []}
            ]}
        ]}
    ]),

    %% Inicia o servidor HTTP na porta 8080
    {ok, _} = cowboy:start_clear(http, [
        {port, 8080}
    ]),

    %% Configura as rotas do servidor
    cowboy_router:unset_env(http, Dispatch),

    %% Inicia a alocação de recursos
    allocate_resources().

%% Função para lidar com a criação de uma pessoa
handle_create(Req, State) ->
    {ok, Body, Req2} = cowboy_req:body(Req),
    case jsx:decode(Body) of
        {ok, Data} ->
            %% Aqui você terá o objeto decodificado da requisição JSON
            %% Agora você pode usar o ID para salvar a pessoa no banco de dados, por exemplo
            io:format("Create person: ~p~n", [Data]),
            {ok, Req2, State};
        {error, Reason} ->
            io:format("JSON decoding error: ~p~n", [Reason]),
            {bad_request, Req2, State}
    end.

%% Alocação de recursos
allocate_resources() ->
    Monitor = spawn_link(fun monitor/0),
    loop(Monitor).

%% Monitora o uso da CPU e memória
monitor() ->
    {ok, Pid} = os:cmd("top -n 1 -b | awk '/^%Cpu/{print $2}'"),
    CpuUsage = list_to_float(string:strip(Pid)),
    {ok, Pid2} = os:cmd("free | awk '/Mem/{print $3/$2}'"),
    MemoryUsage = list_to_float(string:strip(Pid2)),

    %% Atualiza os recursos alocados
    NewResources = update_resources(CpuUsage, MemoryUsage),

    %% Verifica se é necessário o escalonamento automático
    case NewResources of
        true -> spawn_link(fun auto_scale/0);
        _ -> ok
    end,

    %% Aguarda um intervalo de tempo antes de monitorar novamente
    timer:sleep(5000),
    monitor().

%% Atualiza os recursos alocados
update_resources(CpuUsage, MemoryUsage) ->
    TotalCpus = ?TOTAL_CPUS,
    TotalMemory = ?TOTAL_MEMORY,
    NewAllocatedCpus = case CpuUsage >= ?CPU_USAGE_THRESHOLD of
        true -> 0.0;
        false -> TotalCpus
    end,
    NewAllocatedMemory = case MemoryUsage >= ?MEMORY_USAGE_THRESHOLD of
        true -> 0.0;
        false -> TotalMemory
    end,
    NewResources = #resources{
        allocated_cpus = NewAllocatedCpus,
        allocated_memory = NewAllocatedMemory
    },
    io:format("Updated resources: ~p~n", [NewResources]),
    NewResources.

%% Função de escalonamento automático
auto_scale() ->
    %% Lógica de escalonamento automático aqui
    io:format("Automatic scaling executed~n").

%%% pessoas_handler.erl %%%

-module(pessoas_handler).
-export([handle_create/2]).

-include_lib("cowboy/include/cowboy.hrl").

%% Função para lidar com a criação de uma pessoa
handle_create(Req, State) ->
    {ok, Body, Req2} = cowboy_req:body(Req),
    case jsx:decode(Body) of
        {ok, Data} ->
            %% Aqui você terá o objeto decodificado da requisição JSON
            %% Agora você pode usar o ID para salvar a pessoa no banco de dados, por exemplo
            io:format("Create person: ~p~n", [Data]),
            {ok, Req2, State};
        {error, Reason} ->
            io:format("JSON decoding error: ~p~n", [Reason]),
            {bad_request, Req2, State}
   Aqui está a nova versão da API com os endpoints comentados e a atualização na alocação de recursos:

```erlang
%%% api.erl %%%

-module(api).
-export([start/0, allocate_resources/0]).

%% Variáveis de alocação de recursos
-define(TOTAL_CPUS, 1.5).
-define(TOTAL_MEMORY, 3000).

%% Variáveis de uso de recursos
-define(CPU_USAGE_THRESHOLD, 0.7).
-define(MEMORY_USAGE_THRESHOLD, 0.8).

%% Estado de alocação de recursos
-record(resources, {
    allocated_cpus = 0.0,
    allocated_memory = 0.0
}).

%% Inicia o servidor
start() ->
    Dispatch = cowboy_router:compile([
        {'_', [
            %% Endpoint para criar uma pessoa
            {"/pessoas", cowboy_rest, [
                {"/", handle_create, []}
            ]}
        ]}
    ]),

    %% Inicia o servidor HTTP na porta 8080
    {ok, _} = cowboy:start_clear(http, [
        {port, 8080}
    ]),

    %% Configura as rotas do servidor
    cowboy_router:unset_env(http, Dispatch),

    %% Inicia a alocação de recursos
    allocate_resources().

%% Função para lidar com a criação de uma pessoa
handle_create(Req, State) ->
    {ok, Body, Req2} = cowboy_req:body(Req),
    case jsx:decode(Body) of
        {ok, Data} ->
            %% Aqui você terá o objeto decodificado da requisição JSON
            %% Agora você pode usar o ID para salvar a pessoa no banco de dados, por exemplo
            io:format("Create person: ~p~n", [Data]),
            {ok, Req2, State};
        {error, Reason} ->
            io:format("JSON decoding error: ~p~n", [Reason]),
            {bad_request, Req2, State}
    end.

%% Alocação de recursos
allocate_resources() ->
    Monitor = spawn_link(fun monitor/0),
    loop(Monitor).

%% Monitora o uso da CPU e memória
monitor() ->
    {ok, Pid} = os:cmd("top -n 1 -b | awk '/^%Cpu/{print $2}'"),
    CpuUsage = list_to_float(string:strip(Pid)),
    {ok, Pid2} = os:cmd("free | awk '/Mem/{print $3/$2}'"),
    MemoryUsage = list_to_float(string:strip(Pid2)),

    %% Atualiza os recursos alocados
    NewResources = update_resources(CpuUsage, MemoryUsage),

    %% Verifica se é necessário o escalonamento automático
    case NewResources of
        true -> spawn_link(fun auto_scale/0);
        _ -> ok
    end,

    %% Aguarda um intervalo de tempo antes de monitorar novamente
    timer:sleep(5000),
    monitor().

%% Atualiza os recursos alocados
update_resources(CpuUsage, MemoryUsage) ->
    TotalCpus = ?TOTAL_CPUS,
    TotalMemory = ?TOTAL_MEMORY,
    NewAllocatedCpus = case CpuUsage >= ?CPU_USAGE_THRESHOLD of
        true -> 0.0;
        false -> TotalCpus
    end,
    NewAllocatedMemory = case MemoryUsage >= ?MEMORY_USAGE_THRESHOLD of
        true -> 0.0;
        false -> TotalMemory
    end,
    NewResources = #resources{
        allocated_cpus = NewAllocatedCpus,
        allocated_memory = NewAllocatedMemory
    },
    io:format("Updated resources: ~p~n", [NewResources]),
    NewResources.

%% Função de escalonamento automático
auto_scale() ->
    %% Lógica de escalonamento automático aqui
    io:format("Automatic scaling executed~n").

%%% pessoas_handler.erl %%%

-module(pessoas_handler).
-export([handle_create/2]).

-include_lib("cowboy/include/cowboy.hrl").

%% Função para lidar com a criação de uma pessoa
handle_create(Req, State) ->
    {ok, Body, Req2} = cowboy_req:body(Req),
    case jsx:decode(Body) of
        {ok, Data} ->
            %% Aqui você terá o objeto decodificado da requisição JSON
            %% Agora você pode usar o ID para salvar a pessoa no banco de dados, por exemplo
            io:format("Create person: ~p~n", [Data]),
            {ok, Req2, State};
        {error, Reason} ->
            io:format("JSON decoding error: ~p~n", [Reason]),
            {bad]