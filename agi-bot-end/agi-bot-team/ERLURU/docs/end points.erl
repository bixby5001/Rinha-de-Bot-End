Aqui estão os endpoints curl públicos para cada região:

**Região 1:**
```bash
# Obter todas as pessoas
curl -X GET https://8081-cs-639514086775-default.cs-us-east1-pkhd.cloudshell.dev/pessoas

# Criar uma pessoa
curl -X POST https://8081-cs-639514086775-default.cs-us-east1-pkhd.cloudshell.dev/pessoas -H "Content-Type: application/json" -d '{"nome": "João", "idade": 25}'

# Obter uma pessoa por ID
curl -X GET https://8081-cs-639514086775-default.cs-us-east1-pkhd.cloudshell.dev/pessoas/1

# Atualizar uma pessoa por ID
curl -X PUT https://8081-cs-639514086775-default.cs-us-east1-pkhd.cloudshell.dev/pessoas/1 -H "Content-Type: application/json" -d '{"nome": "João Silva", "idade": 26}'

# Excluir uma pessoa por ID
curl -X DELETE https://8081-cs-639514086775-default.cs-us-east1-pkhd.cloudshell.dev/pessoas/1
```

**Região 2:**
```bash
# Obter todas as pessoas
curl -X GET https://3000-cs-b1fa01a8-77d1-4331-8e10-a1c0717d189d.cs-us-east1-pkhd.cloudshell.dev/pessoas

# Criar uma pessoa
curl -X POST https://3000-cs-b1fa01a8-77d1-4331-8e10-a1c0717d189d.cs-us-east1-pkhd.cloudshell.dev/pessoas -H "Content-Type: application/json" -d '{"nome": "Maria", "idade": 30}'

# Obter uma pessoa por ID
curl -X GET https://3000-cs-b1fa01a8-77d1-4331-8e10-a1c0717d189d.cs-us-east1-pkhd.cloudshell.dev/pessoas/2

# Atualizar uma pessoa por ID
curl -X PUT https://3000-cs-b1fa01a8-77d1-4331-8e10-a1c0717d189d.cs-us-east1-pkhd.cloudshell.dev/pessoas/2 -H "Content-Type: application/json" -d '{"nome": "Maria Souza", "idade": 31}'

# Excluir uma pessoa por ID
curl -X DELETE https://3000-cs-b1fa01a8-77d1-4331-8e10-a1c0717d189d.cs-us-east1-pkhd.cloudshell.dev/pessoas/2
```

**Região 3:**
```bash
# Obter todas as pessoas
curl -X GET https://8080-cs-1002024752404-default.cs-us-east1-pkhd.cloudshell.dev/pessoas

# Criar uma pessoa
curl -X POST https://8080-cs-1002024752404-default.cs-us-east1-pkhd.cloudshell.dev/pessoas -H "Content-Type: application/json" -d '{"nome": "Pedro", "idade": 35}'

# Obter uma pessoa por ID
curl -X GET https://8080-cs-1002024752404-default.cs-us-east1-pkhd.cloudshell.dev/pessoas/3

# Atualizar uma pessoa por ID
curl -X PUT https://8080-cs-1002024752404-default.cs-us-east1-pkhd.cloudshell.dev/pessoas/3 -H "Content-Type: application/json" -d '{"nome": "Pedro Santos", "idade": 36}'

# Excluir uma pessoa por ID
curl -X DELETE https://8080-cs-1002024752404-default.cs-us-east1-pkhd.cloudshell.dev/pessoas/3
```