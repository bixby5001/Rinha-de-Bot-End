{deps, [
    {cowboy, "2.9.0"},
    {jsx, "3.2.0"}
]}.

- api
  - api.erl
  - api.hrl
- pessoas_handler
  - pessoas_handler.erl
- recursos_handler
  - recursos_handler.erl


...
-include("api.hrl").

%% Monitora o uso de CPU e memória
monitor() ->
    {ok, Pid} = os:cmd("top -n 1 -b | awk '/^%Cpu/{print $2}'"),
    CpuUsage = list_to_float(string:strip(Pid)),
    {ok, Pid2} = os:cmd("free | awk '/Mem/{print $3/$2}'"),
    MemoryUsage = list_to_float(string:strip(Pid2)),

    %% Atualiza os recursos alocados
    NewResources = update_resources(CpuUsage, MemoryUsage),

    %% Verifica se é necessário fazer o escalonamento automático
    ...
...

...
-include("api.hrl").

%% Função para lidar com a criação de uma pessoa
handle_create(Req, State) ->
    {ok, Body, Req2} = cowboy_req:body(Req),
    case jsx:decode(Body) of
        {ok, Data} ->
            %% Aqui você terá o objeto decodificado da requisição JSON
            %% Agora você pode usar o ID para salvar a pessoa no banco de dados, por exemplo
            io:format("Criar pessoa: ~p~n", [Data]),
            {ok, Req2, State};
        {error, Reason} ->
            io:format("Erro na decodificação JSON: ~p~n", [Reason]),
            {bad_request, Req2, State}
    end.
...


%% api.erl
-module(api).
-export([start/0]).

-include_lib("cowboy/include/cowboy.hrl").
-include_lib("jsx/include/jsx.hrl").

%% pessoas_handler.erl
-module(pessoas_handler).
-export([handle_create/2, handle_get/2, handle_update/2, handle_delete/2]).

-include_lib("cowboy/include/cowboy.hrl").
-include_lib("jsx/include/jsx.hrl").

%% recursos_handler.erl
-module(recursos_handler).
-export([allocate_resources/0, monitor/0]).

-include_lib("cowboy/include/cowboy.hrl").




-module(api).
-export([get_endpoint/1]).

%% Configurações das regiões
-define(REGIAO1, "https://8081-cs-639514086775-default.cs-us-east1-pkhd.cloudshell.dev/").
-define(REGIAO2, "https://3000-cs-b1fa01a8-77d1-4331-8e10-a1c0717d189d.cs-us-east1-pkhd.cloudshell.dev/").
-define(REGIAO3, "https://8080-cs-1002024752404-default.cs-us-east1-pkhd.cloudshell.dev/").

%% Função para obter o endpoint da região
get_endpoint(1) ->
    ?REGIAO1;
get_endpoint(2) -> 
    ?REGIAO2;
get_endpoint(3) ->
    ?REGIAO3;
get_endpoint(_) ->
    %% Endpoint padrão caso a região não seja encontrada
    "https://localhost:8080/".


-module(api).
-export([start/0, allocate_resources/0]).

%% Resource allocation variables
-define(TOTAL_CPUS, 1.5).
-define(TOTAL_MEMORY, 3000).

%% Resource usage variables
-define(CPU_USAGE_THRESHOLD, 0.7).
-define(MEMORY_USAGE_THRESHOLD, 0.8).

%% Resource allocation state
-record(resources, {
    allocated_cpus = 0.0,
    allocated_memory = 0.0
}).

%% Inicia o servidor
start() ->
    Dispatch = cowboy_router:compile([
        {'_', [
            {"/pessoas", cowboy_rest, [
                {"/", handle_create, []}
            ]},
            {"/pessoas/:id", cowboy_rest, [
                {"/", handle_get, []},
                {"/", handle_update, []},
                {"/", handle_delete, []}
            ]}
        ]}
    ]),

    %% Inicia o servidor HTTP na porta 8080
    {ok, _} = cowboy:start_clear(http, [
        {port, 8080}
    ]),

    %% Define as rotas do servidor
    cowboy_router:unset_env(http, Dispatch),

    %% Inicia o monitoramento de recursos
    allocate_resources().

%% Função para lidar com a criação de uma pessoa
handle_create(Req, State) ->
    {ok, Body, Req2} = cowboy_req:body(Req),
    case jsx:decode(Body) of
        {ok, Data} ->
            %% Aqui você terá o objeto decodificado da requisição JSON
            %% Agora você pode usar o ID para salvar a pessoa no banco de dados, por exemplo
            io:format("Criar pessoa: ~p~n", [Data]),
            {ok, Req2, State};
        {error, Reason} ->
            io:format("Erro na decodificação JSON: ~p~n", [Reason]),
            {bad_request, Req2, State}
    end.

%% Função para lidar com a obtenção de uma pessoa pelo ID
handle_get(Req, State) ->
    Id = cowboy_req:binding(id, Req),
    %% Aqui você terá o ID da pessoa a ser buscada
    %% Agora você pode buscar a pessoa no banco de dados pelo ID, por exemplo
    io:format("Obter pessoa por ID: ~p~n", [Id]),
    {ok, Req, State}.

%% Função para lidar com a atualização de uma pessoa pelo ID
handle_update(Req, State) ->
    Id = cowboy_req:binding(id, Req),
    {ok, Body, Req2} = cowboy_req:body(Req),
    case jsx:decode(Body) of
        {ok, Data} ->
            %% Aqui você terá o objeto decodificado da requisição JSON
            %% Agora você pode usar o ID para atualizar a pessoa no banco de dados, por exemplo
            io:format("Atualizar pessoa por ID: ~p, Data: ~p~n", [Id, Data]),
            {ok, Req2, State};
        {error, Reason} ->
            io:format("Erro na decodificação JSON: ~p~n", [Reason]),
            {bad_request, Req2, State}
    end.

%% Função para lidar com a exclusão de uma pessoa pelo ID
handle_delete(Req, State) ->
    Id = cowboy_req:binding(id, Req),
    %% Aqui você terá o ID da pessoa a ser excluída
    %% Agora você pode excluir a pessoa do banco de dados pelo ID, por exemplo
    io:format("Excluir pessoa por ID: ~p~n", [Id]),
    {no_content, Req, State}.

%% Monitora o uso de CPU e memória
monitor() ->
    {ok, Pid} = os:cmd("top -n 1 -b | awk '/^%Cpu/{print $2}'"),
    CpuUsage = list_to_float(string:strip(Pid)),
    {ok, Pid2} = os:cmd("free | awk '/Mem/{print $3/$2}'"),
    MemoryUsage = list_to_float(string:strip(Pid2)),

    %% Atualiza os recursos alocados
    NewResources = update_resources(CpuUsage, MemoryUsage),

    %% Verifica se é necessário fazer o escalonamento automHere's an updated version of the code that includes a simple resource allocation mechanism:

```erlang
-module(api).
-export([start/0, allocate_resources/0]).

%% Resource allocation variables
-define(TOTAL_CPUS, 1.5).
-define(TOTAL_MEMORY, 3000).

%% Resource usage variables
-define(CPU_USAGE_THRESHOLD, 0.7).
-define(MEMORY_USAGE_THRESHOLD, 0.8).

%% Resource allocation state
-record(resources, {
    allocated_cpus = 0.0,
    allocated_memory = 0.0
}).

%% Inicia o servidor
start() ->
    Dispatch = cowboy_router:compile([
        {'_', [
            {"/pessoas", cowboy_rest, [
                {"/", handle_create, []}
            ]},
            {"/pessoas/:id", cowboy_rest, [
                {"/", handle_get, []},
                {"/", handle_update, []},
                {"/", handle_delete, []}
            ]}
        ]}
    ]),

    %% Inicia o servidor HTTP na porta 8080
    {ok, _} = cowboy:start_clear(http, [
        {port, 8080}
    ]),

    %% Define as rotas do servidor
    cowboy_router:unset_env(http, Dispatch),

    %% Inicia o monitoramento de recursos
    allocate_resources().

%% Função para lidar com a criação de uma pessoa
handle_create(Req, State) ->
    {ok, Body, Req2} = cowboy_req:body(Req),
    case jsx:decode(Body) of
        {ok, Data} ->
            %% Aqui você terá o objeto decodificado da requisição JSON
            %% Agora você pode usar o ID para salvar a pessoa no banco de dados, por exemplo
            io:format("Criar pessoa: ~p~n", [Data]),
            {ok, Req2, State};
        {error, Reason} ->
            io:format("Erro na decodificação JSON: ~p~n", [Reason]),
            {bad_request, Req2, State}
    end.

%% Função para lidar com a obtenção de uma pessoa pelo ID
handle_get(Req, State) ->
    Id = cowboy_req:binding(id, Req),
    %% Aqui você terá o ID da pessoa a ser buscada
    %% Agora você pode buscar a pessoa no banco de dados pelo ID, por exemplo
    io:format("Obter pessoa por ID: ~p~n", [Id]),
    {ok, Req, State}.

%% Função para lidar com a atualização de uma pessoa pelo ID
handle_update(Req, State) ->
    Id = cowboy_req:binding(id, Req),
    {ok, Body, Req2} = cowboy_req:body(Req),
    case jsx:decode(Body) of
        {ok, Data} ->
            %% Aqui você terá o objeto decodificado da requisição JSON
            %% Agora você pode usar o ID para atualizar a pessoa no banco de dados, por exemplo
            io:format("Atualizar pessoa por ID: ~p, Data: ~p~n", [Id, Data]),
            {ok, Req2, State};
        {error, Reason} ->
            io:format("Erro na decodificação JSON: ~p~n", [Reason]),
            {bad_request, Req2, State}
    end.

%% Função para lidar com a exclusão de uma pessoa pelo ID
handle_delete(Req, State) ->
    Id = cowboy_req:binding(id, Req),
    %% Aqui você terá o ID da pessoa a ser excluída
    %% Agora você pode excluir a pessoa do banco de dados pelo ID, por exemplo
    io:format("Excluir pessoa por ID: ~p~n", [Id]),
    {no_content, Req, State}.

%% Monitora o uso de CPU e memória
monitor() ->
    {ok, Pid} = os:cmd("top -n 1 -b | awk '/^%Cpu/{print $2}'"),
    CpuUsage = list_to_float(string:strip(Pid)),
    {ok, Pid2} = os:cmd("free | awk '/Mem/{print $3/$2}'"),
    MemoryUsage = list_to_float(string:strip(Pid2)),

    %% Atualiza os recursos alocados
    NewResources = update_resources(CpuUsage, MemoryUsage),

    %% Verifica se é necessário fazer o escalonamento automático
    case {CpuUsage > ?CPU_USAGE_THRESHOLD, MemoryUsage > ?MEMORY_USAGE_THRESHOLD} of
        {true, _} ->
            %% Adiciona mais instâncias do servidor
            scale_up();
        {_, true} ->
            %% Adiciona mais


Endpoints;

Claro! Aqui estão as URLs completas em curl para cada região:

Região 1:
```
curl -X GET https://8081-cs-639514086775-default.cs-us-east1-pkhd.cloudshell.dev/pessoas
curl -X POST https://8081-cs-639514086775-default.cs-us-east1-pkhd.cloudshell.dev/pessoas -H "Content-Type: application/json" -d '{"nome": "João", "idade": 25}'
curl -X GET https://8081-cs-639514086775-default.cs-us-east1-pkhd.cloudshell.dev/pessoas/1
curl -X PUT https://8081-cs-639514086775-default.cs-us-east1-pkhd.cloudshell.dev/pessoas/1 -H "Content-Type: application/json" -d '{"nome": "João Silva", "idade": 26}'
curl -X DELETE https://8081-cs-639514086775-default.cs-us-east1-pkhd.cloudshell.dev/pessoas/1
```

Região 2:
```
curl -X GET https://3000-cs-b1fa01a8-77d1-4331-8e10-a1c0717d189d.cs-us-east1-pkhd.cloudshell.dev/pessoas
curl -X POST https://3000-cs-b1fa01a8-77d1-4331-8e10-a1c0717d189d.cs-us-east1-pkhd.cloudshell.dev/pessoas -H "Content-Type: application/json" -d '{"nome": "Maria", "idade": 30}'
curl -X GET https://3000-cs-b1fa01a8-77d1-4331-8e10-a1c0717d189d.cs-us-east1-pkhd.cloudshell.dev/pessoas/2
curl -X PUT https://3000-cs-b1fa01a8-77d1-4331-8e10-a1c0717d189d.cs-us-east1-pkhd.cloudshell.dev/pessoas/2 -H "Content-Type: application/json" -d '{"nome": "Maria Souza", "idade": 31}'
curl -X DELETE https://3000-cs-b1fa01a8-77d1-4331-8e10-a1c0717d189d.cs-us-east1-pkhd.cloudshell.dev/pessoas/2
```

Região 3:
```
curl -X GET https://8080-cs-1002024752404-default.cs-us-east1-pkhd.cloudshell.dev/pessoas
curl -X POST https://8080-cs-1002024752404-default.cs-us-east1-pkhd.cloudshell.dev/pessoas -H "Content-Type: application/json" -d '{"nome": "Pedro", "idade": 35}'
curl -X GET https://8080-cs-1002024752404-default.cs-us-east1-pkhd.cloudshell.dev/pessoas/3
curl -X PUT https://8080-cs-1002024752404-default.cs-us-east1-pkhd.cloudshell.dev/pessoas/3 -H "Content-Type: application/json" -d '{"nome": "Pedro Santos", "idade": 36}'
curl -X DELETE https://8080-cs-1002024752404-default.cs-us-east1-pkhd.cloudshell.dev/pessoas/3
```

Lembre-se de substituir as informações relevantes, como IDs, nomes e dados JSON, ao usar esses comandos curl.

-module(api_tests).
-include_lib("eunit/include/eunit.hrl").

%% Teste para a função handle_create
handle_create_test() ->
    Req = some_request_data(),
    State = some_state_data(),
    ?assertEqual({ok, Req2, State2}, api:handle_create(Req, State)),
    %% Adicione mais asserções de teste para outras funções do módulo api
    ok.

%% Função para gerar dados de requisição fictícia
some_request_data() ->
    %% Implemente a geração de dados de requisição fictícia aqui
    Req.

%% Função para gerar dados de estado fictício
some_state_data() ->
    %% Implemente a geração de dados de estado fictício aqui
    State.

-module(recursos_handler_tests).
-include_lib("eunit/include/eunit.hrl").

%% Teste para a função allocate_resources
allocate_resources_test() ->
    ?assertEqual(ExpectedResult, recursos_handler:allocate_resources()),
    %% Adicione mais asserções de teste para outras funções do módulo recursos_handler
    ok.
