 **Aqui está o código atualizado da aplicação MulherMaravilha com os módulos pessoas_service e system_monitor, com documentação:**

**main.erl**

```erlang
-module(main).
-export([start/0]).

% Função principal que inicia os serviços da aplicação
start() ->
    start_recursos_service(),
    start_api_service(),
    start_system_monitor(),
    ok.
```

**recursos_service.erl**

```erlang
-module(recursos_service).
-export([start/0, monitor_resources/0, auto_scale/0, scale_out/0, scale_in/0]).

% Inicia o serviço de recursos, iniciando os processos de monitoramento e auto-scaling
start() ->
    spawn_link(fun() -> monitor_resources() end),
    spawn_link(fun() -> auto_scale() end),
    ok.

% Monitora os recursos da aplicação (CPU, memória, etc.)
monitor_resources() ->
    cpu_usage = cpu_monitor(),
    memory_usage = memory_monitor(),
    scale_if_needed(cpu_usage, memory_usage).

% Executa o auto-scaling baseado nos dados de monitoramento
auto_scale() ->
    receive
        {cpu_usage, CpuUsage} ->
            if
                CpuUsage > 0.7 ->
                    scale_out();
                true ->
                    scale_in()
            end;
        {memory_usage, MemoryUsage} ->
            if
                MemoryUsage > 1024 ->
                    scale_out();
                true ->
                    scale_in()
            end
    end.

% Adiciona uma nova instância da aplicação
scale_out() ->
    spawn_link(fun() -> start_new_instance() end).

% Remove uma instância da aplicação
scale_in() ->
    shutdown_instance().

% Inicia uma nova instância da aplicação
start_new_instance() ->
    {ok, Pid} = cowboy:start_clear(http, []),
    {ok, Pid} = recursos_service:start(),
    ok.

% Encerra uma instância da aplicação
shutdown_instance() ->
    Pid = resources_service:get_pid(),
    cowboy:stop(Pid).
```

**api_service.erl**

```erlang
-module(api_service).
-export([start/0, handle_create/2]).

% Inicia o serviço de API, iniciando um processo para lidar com requisições
start() ->
    spawn_link(fun() -> handle_requests() end),
    ok.

% Lida com as requisições HTTP recebidas
handle_requests() ->
    receive
        {cowboy, Req, _} ->
            {ok, Req2} = cowboy_req:parse_json(Req),
            {ok, _} = handle_create(Req2),
            cowboy:reply(200, Req2, [])
    end.

% Trata a requisição de criação de uma pessoa
handle_create(Req) ->
    {PersonId, Person} = jsx:decode(Req),
    {ok, Person} = pessoas_service:create(PersonId, Person),
    {ok, Person}.
```

**pessoas_service.erl**

```erlang
-module(pessoas_service).
-export([create/2]).

% Cria um novo registro de pessoa
create(PersonId, Person) ->
    % Implemente a lógica de criação de pessoa aqui
    ok.
```

**system_monitor.erl**

```erlang
-module(system_monitor).
-export([cpu_monitor/0, memory_monitor/0, net_io_monitor/0, block_io_monitor/0, pids_monitor/0, gflops_watts_monitor/0]).

% Funções para monitoramento de recursos:
% - cpu_monitor/0
% - memory_monitor/0
% - net_io_monitor/0
% - block_io_monitor/0
% - pids_monitor/0
% - gflops_watts_monitor/0
%