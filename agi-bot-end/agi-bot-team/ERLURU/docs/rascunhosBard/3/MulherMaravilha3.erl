**Diagrama de MulherMaravilha com 3 regiões GCP**

```
                                                                                    MulherMaravilha
                                                                                        /\
                                                                                     /   \
                                                                                   /     \
                                                                                  /       \
                                                                                /         \
                                                                              /           \
                                                                            /             \
                                                                           /              \
                                                                          /               \
                                                                         /                \
                                                                        /                  \
                                                                       /                   \
                                                                      /                    \
                                                                     /                     \
                                                                    /                      \
                                                                   /                       \
                                                                  /                        \
                                                                 /                         \
                                                                /                          \
                                                              /                           \
                                                             /                            \
                                                            /                             \
                                                           /                              \
                                                          /                               \
                                                         /                                \
                                                        /                                 \
                                                       /                                  \
                                                      /                                   \
                                                     /                                    \
                                                    /                                     \
                                                   /                                      \
                                                  /                                       \
                                                 /                                        \
                                                /                                         \
                                               /                                          \
                                              /                                           \
                                             /                                            \
                                            /                                             \
                                           /                                              \
                                          /                                               \
                                         /                                                \
                                        /                                                 \
                                       /                                                  \
                                      /                                                   \
                                     /                                                    \
                                    /                                                     \
                                   /                                                      \
                                  /                                                       \
                                 /                                                        \
                                /                                                         \
                               /                                                          \
                              /                                                           \
                             /                                                            \
                            /                                                             \
                           /                                                              \
                          /                                                               \
                         /                                                                \
                        /                                                                 \
                       /                                                                  \
                      /                                                                   \
                     /                                                                    \
                    /                                                                     \
                   /                                                                      \
                  /                                                                       \
                 /                                                                        \
                /                                                                         \
               /                                                                          \
              /                                                                           \
             /                                                                            \
            /                                                                             \
           /                                                                              \
          /                                                                               \
         /                                                                                \
        /                                                                                 \
       /                                                                                  \
      /                                                                                   \
     /                                                                                    \
    /                                                                                     \
   /                                                                                      \
  /                                                                                       \
 /                                                                                        \
/                                                                                         \
```

**Explicação do diagrama:**

* A aplicação MulherMaravilha é iniciada em três regiões GCP: **us-central1**, **us-west1** e **europe-west1**.
* Cada região possui duas APIs, uma para **pessoas** e outra para **system_monitor**.
* As APIs são 100% in memory, sem uso de banco de dados. A redundância, ACID e segurança são garantidos pelas três regiões geograficamente distribuídas.

**Aplicação MulherMaravilha na árvore de módulos:**

**main:** Módulo principal responsável por iniciar os outros módulos.

**recursos_service:** Módulo responsável pelo monitoramento de recursos e auto-scaling.

**api_service:** Módulo responsável pela API RESTful.

**pessoas_service:** Módulo responsável pelo gerenciamento de dados de pessoas