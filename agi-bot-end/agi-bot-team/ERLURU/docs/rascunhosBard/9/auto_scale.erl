auto_scale() ->
    receive
        {cpu_usage, CpuUsage} ->
            CpuUsageThreshold = mulher_maravilha_config:get_cpu_usage_threshold(),
            if
                CpuUsage > CpuUsageThreshold ->
                    scale_out();
                true ->
                    scale_in()
            end;
        {memory_usage, MemoryUsage} ->
            MemoryUsageThreshold = mulher_maravilha_config:get_memory_usage_threshold(),
            if
                MemoryUsage > MemoryUsageThreshold ->
                    scale_out();
                true ->
                    scale_in()
            end
    end.
