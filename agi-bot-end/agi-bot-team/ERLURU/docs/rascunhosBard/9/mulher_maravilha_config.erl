-module(mulher_maravilha_config).
-export([get_initial_regions/0,
     get_initial_instances_per_region/0,
     get_cpu_usage_threshold/0,
     get_memory_usage_threshold/0]).

get_initial_regions() ->
 ["us-central1", "us-east1", "europe-west1"].

get_initial_instances_per_region() ->
 2.

get_cpu_usage_threshold() ->
 0.7.

get_memory_usage_threshold() ->
 1024.
