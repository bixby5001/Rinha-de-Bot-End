monitor_resources() ->
    CpuUsageThreshold = mulher_maravilha_config:get_cpu_usage_threshold(),
    MemoryUsageThreshold = mulher_maravilha_config:get_memory_usage_threshold(),
    cpu_usage = cpu_monitor(),
    memory_usage = memory_monitor(),
    scale_if_needed(cpu_usage, memory_usage, CpuUsageThreshold, MemoryUsageThreshold).
