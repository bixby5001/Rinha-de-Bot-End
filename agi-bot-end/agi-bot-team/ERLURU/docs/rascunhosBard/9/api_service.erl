-module(api_service).
-export([start/0, handle_create/2]).

start() ->
  spawn_link(fun() -> handle_requests() end),
  ok.

handle_requests() ->
  receive
    {cowboy, Req, _} ->
      {ok, Req2} = cowboy_req:parse_json(Req),
      {ok, _} = handle_create(Req2),
      cowboy:reply(200, Req2, [])
  end.

handle_create(Req) ->
  {PersonId, Person} = jsx:decode(Req),
  {ok, Person} = pessoas_service:create(PersonId, Person),
  {ok, Person}.
