-define(GCP_ACCOUNTS, ["conta-1", "conta-2", "conta-3"]).

%%@doc Inicia os serviços nas três regiões
iniciar_servicos_regiao(Regiao) ->
  io:format("Iniciando serviços na região ~s~n", [Regiao]),
  pessoas_service:start(Regiao),
  api_service:start(Regiao),
  recursos_service:monitor_resources(Regiao),
  start_gcp_auth(Regiao).

%%@doc Inicia o serviço de autenticação OAuth
start_gcp_auth(Regiao) ->
  io:format("Iniciando serviço de autenticação OAuth na região ~s~n", [Regiao]),
  {ok, Auth} = gcp_auth:start(Regiao, ?GCP_ACCOUNTS),
  {ok, _} = Auth:authenticate(),
  ok.
