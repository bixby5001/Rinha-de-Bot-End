-module(mulher_maravilha).
-export([start/0, handle_request/1]).

start() ->
 spawn_link(fun() -> monitor_resources() end),
 spawn_link(fun() -> auto_scale() end),
 start_api().

handle_request(Req) ->
 {ok, Req2} = cowboy_req:parse_json(Req),
 {ok, _} = handle_operation(Req2),
 cowboy:reply(200, Req2, []).

handle_operation(Req) ->
 operation = Req["operation"],
 case operation of
   "create_person" ->
      {ok, Person} = people_service:create(Req["person"]),
      {ok, Person};
   "other_operation" ->
      {ok, Result} = other_service:do_something(Req["parameters"]),
      {ok, Result}
  end.

monitor_resources() ->
 cpu_usage = cpu_monitor(),
 memory_usage = memory_monitor(),
 scale_if_needed(cpu_usage, memory_usage).

auto_scale() ->
 receive
  {cpu_usage, CpuUsage} ->
   if
    CpuUsage > 0.7 ->
     scale_out();
    true ->
     scale_in()
   end;
  {memory_usage, MemoryUsage} ->
   if
    MemoryUsage > 1024 ->
     scale_out();
    true ->
     scale_in()
   end
 end.

scale_out() ->
 spawn_link(fun() -> start_new_instance() end).

scale_in() ->
 shutdown_instance().

start_new_instance() ->
 {ok, Pid} = cowboy:start_clear(http, []),
 {ok, Pid} = recursos_service:start(),
 ok.

shutdown_instance() ->
 Pid = resources_service:get_pid(),
 cowboy:stop(Pid).
