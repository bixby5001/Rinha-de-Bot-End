-module(system_monitor).
-export([start/0, monitor_resources/0, cpu_monitor/0, memory_monitor/0, net_io_monitor/0, block_io_monitor/0, pids_monitor/0, gflops_watts_monitor/0]).

start() ->
  spawn_link(fun() -> monitor_resources() end).

monitor_resources() ->
  cpu_usage = cpu_monitor(),
  memory_usage = memory_monitor(),
  net_io = net_io_monitor(),
  block_io = block_io_monitor(),
  pids = pids_monitor(),
  gflops_watts = gflops_watts_monitor(),
  recursos_service:notify_resource_usage(cpu_usage, memory_usage, net_io, block_io, pids, gflops_watts),
  timer:sleep(1000), % Monitorar a cada 1 segundo
  monitor_resources().

cpu_monitor() ->
  % Implementar a lógica para obter o uso da CPU
  % Retornar a porcentagem de uso da CPU
  0.5. % Exemplo

memory_monitor() ->
  % Implementar a lógica para obter o uso da memória
  % Retornar a porcentagem de uso da memória
  0.3. % Exemplo

net_io_monitor() ->
  % Implementar a lógica para obter o uso de rede
  % Retornar os dados de uso de rede
  1024. % Exemplo

block_io_monitor() ->
  % Implementar a lógica para obter o uso de I/O em bloco
  % Retornar os dados de uso de I/O em bloco
  512. % Exemplo

pids_monitor() ->
  % Implementar a lógica para obter a quantidade de processos
  % Retornar o número de processos
  10. % Exemplo

gflops_watts_monitor() ->
  % Implementar a lógica para obter o desempenho em GFlops/Watts
  % Retornar o valor em GFlops/Watts
  0.8. % Exemplo
