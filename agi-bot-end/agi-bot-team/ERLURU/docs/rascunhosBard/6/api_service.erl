-module(api_service).
-export([start/0, handle_create/2]).

% Inicia o serviço de API, iniciando um processo para lidar com requisições
start() ->
    spawn_link(fun() -> handle_requests() end),
    ok.

% Lida com as requisições HTTP recebidas
handle_requests() ->
    receive
        {cowboy, Req, _} ->
            {ok, Req2} = cowboy_req:parse_json(Req),
            {ok, _} = handle_create(Req2),
            cowboy:reply(200, Req2, [])
    end.

% Trata a requisição de criação de uma pessoa
handle_create(Req) ->
    {PersonId, Person} = jsx:decode(Req),
    {ok, Person} = pessoas_service:create(PersonId, Person),
    {ok, Person}.
