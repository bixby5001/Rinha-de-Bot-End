-module(main).
-export([start/0]).

% Função principal que inicia os serviços da aplicação
start() ->
    start_recursos_service(),
    start_api_service(),
    start_system_monitor(),
    ok.
