-module(recursos_service).
-export([start/0, monitor_resources/0, auto_scale/0, scale_out/0, scale_in/0]).

% Inicia o serviço de recursos, iniciando os processos de monitoramento e auto-scaling
start() ->
    spawn_link(fun() -> monitor_resources() end),
    spawn_link(fun() -> auto_scale() end),
    ok.

% Monitora os recursos da aplicação (CPU, memória, etc.)
monitor_resources() ->
    cpu_usage = cpu_monitor(),
    memory_usage = memory_monitor(),
    scale_if_needed(cpu_usage, memory_usage).

% Executa o auto-scaling baseado nos dados de monitoramento
auto_scale() ->
    receive
        {cpu_usage, CpuUsage} ->
            if
                CpuUsage > 0.7 ->
                    scale_out();
                true ->
                    scale_in()
            end;
        {memory_usage, MemoryUsage} ->
            if
                MemoryUsage > 1024 ->
                    scale_out();
                true ->
                    scale_in()
            end
    end.

% Adiciona uma nova instância da aplicação
scale_out() ->
    spawn_link(fun() -> start_new_instance() end).

% Remove uma instância da aplicação
scale_in() ->
    shutdown_instance().

% Inicia uma nova instância da aplicação
start_new_instance() ->
    {ok, Pid} = cowboy:start_clear(http, []),
    {ok, Pid} = recursos_service:start(),
    ok.

% Encerra uma instância da aplicação
shutdown_instance() ->
    Pid = resources_service:get_pid(),
    cowboy:stop(Pid).
