%% recursos_handler.erl

-module(recursos_handler).
-export([allocate_resources/0, monitor/0, auto_scale/1, scale_out/0, scale_in/0]).

-include_lib("cowboy/include/cowboy.hrl").

-record(resources, {
    allocated_cpus = 0.0,
    allocated_memory = 0.0,
    instances = 2
}).

allocate_resources() ->
    monitor(),
    ok.

monitor() ->
    erlang:monitor(process, {global, cpu_monitor}),
    erlang:monitor(process, {global, memory_monitor}),
    spawn_link(fun() -> cpu_monitor() end),
    spawn_link(fun() -> memory_monitor() end),
    spawn_link(fun() -> auto_scale() end).

cpu_monitor() ->
    {ok, CpuRef} = erlang:open_port({fd, 1, 2}, [{line, 1000}]),
    loop(CpuRef, cpu).

memory_monitor() ->
    {ok, MemoryRef} = erlang:open_port({fd, 1, 2}, [{line, 1000}]),
    loop(MemoryRef, memory).

loop(Port, Type) ->
    receive
        {Port, {data, Data}} ->
            DataTuple = list_to_tuple(Data),
            case Type of
                cpu ->
                    {_, _, _, _, _, _, _, _, _, Cpu} = DataTuple,
                    auto_scale(Cpu);
                memory ->
                    {_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, Memory} = DataTuple,
                    auto_scale(Memory)
            end,
            loop(Port, Type);
        {Port, closed} ->
            ok
    end.

auto_scale(Usage) ->
    case Usage >= hd(?CPU_USAGE_THRESHOLD) of
        true ->
            scale_out();
        false ->
            case Usage =< hd(tl(?CPU_USAGE_THRESHOLD)) of
                true ->
                    scale_in();
                _ ->
                    ok
            end
    end.

scale_out() ->
    %% Execute ações para escalonamento horizontal (adicionar instâncias)
    io:format("Escalonamento horizontal - Adicionando instâncias~n"),
    NewInstances = scale_instances(1),
    update_instances(NewInstances),
    ok.

scale_in() ->
    %% Execute ações para escalonamento horizontal (remover instâncias)
    io:format("Escalonamento horizontal - Removendo instâncias~n"),
    NewInstances = scale_instances(-1),
    update_instances(NewInstances),
    ok.

update_instances(Delta) ->
    %% Atualiza o número de instâncias alocadas
    Resources = #resources{
        instances = instances + Delta
    },
    io:format("Atualizando número de instâncias: ~p~n", [Resources]),
    ok.

scale_instances(Delta) ->
    %% Calcula o novo número de instâncias
    NewInstances = instances + Delta,
    case NewInstances >= 1 of
        true -> NewInstances;
        false -> 1 %% Mantém pelo menos uma instância ativa
    end.

