{deps, [
    {cowboy, "2.9.0"},
    {jsx, "3.2.0"}
]}.

- api
  - api.erl
  - api.hrl
- pessoas_handler
  - pessoas_handler.erl
- recursos_handler
  - recursos_handler.erl

%%% api.erl %%%

-module(api).
-export([start/0, allocate_resources/0]).

%% Resource allocation variables
-define(TOTAL_CPUS, 1.5).
-define(TOTAL_MEMORY, 3000).

%% Resource usage variables
-define(CPU_USAGE_THRESHOLD, 0.7).
-define(MEMORY_USAGE_THRESHOLD, 0.8).

%% Resource allocation state
-record(resources, {
    allocated_cpus = 0.0,
    allocated_memory = 0.0
}).

%% Start the server
start() ->
    Dispatch = cowboy_router:compile([
        {'_', [
            {"/pessoas", cowboy_rest, [
                {"/", handle_create, []}
            ]}
        ]}
    ]),

    %% Start the HTTP server on port 8080
    {ok, _} = cowboy:start_clear(http, [
        {port, 8080}
    ]),

    %% Set the server routes
    cowboy_router:unset_env(http, Dispatch),

    %% Start resource allocation
    allocate_resources().

%% Function to handle person creation
handle_create(Req, State) ->
    {ok, Body, Req2} = cowboy_req:body(Req),
    case jsx:decode(Body) of
        {ok, Data} ->
            %% Here you will have the decoded object from the JSON request
            %% Now you can use the ID to save the person in the database, for example
            io:format("Create person: ~p~n", [Data]),
            {ok, Req2, State};
        {error, Reason} ->
            io:format("JSON decoding error: ~p~n", [Reason]),
            {bad_request, Req2, State}
    end.

%% Resource allocation
allocate_resources() ->
    Monitor = spawn_link(fun monitor/0),
    loop(Monitor).

%% Monitor CPU and memory usage
monitor() ->
    {ok, Pid} = os:cmd("top -n 1 -b | awk '/^%Cpu/{print $2}'"),
    CpuUsage = list_to_float(string:strip(Pid)),
    {ok, Pid2} = os:cmd("free | awk '/Mem/{print $3/$2}'"),
    MemoryUsage = list_to_float(string:strip(Pid2)),

    %% Update allocated resources
    NewResources = update_resources(CpuUsage, MemoryUsage),

    %% Check if automatic scaling is necessary
    case NewResources of
        true -> spawn_link(fun auto_scale/0);
        _ -> ok
    end,

    %% Sleep for a certain interval before monitoring again
    timer:sleep(5000),
    monitor().

%% Update allocated resources
update_resources(CpuUsage, MemoryUsage) ->
    TotalCpus = ?TOTAL_CPUS,
    TotalMemory = ?TOTAL_MEMORY,
    NewAllocatedCpus = case CpuUsage >= ?CPU_USAGE_THRESHOLD of
        true -> 0.0;
        false -> TotalCpus
    end,
    NewAllocatedMemory = case MemoryUsage >= ?MEMORY_USAGE_THRESHOLD of
        true -> 0.0;
        false -> TotalMemory
    end,
    NewResources = #resources{
        allocated_cpus = NewAllocatedCpus,
        allocated_memory = NewAllocatedMemory
    },
    io:format("Updated resources: ~p~n", [NewResources]),
    NewResources.

%% Automatic scaling function
auto_scale() ->
    %% Automatic scaling logic here
    io:format("Automatic scaling executed~n").



%%% recursos_handler.erl %%%

-module(recursos_handler).
-export([allocate_resources/0, monitor/0]).

-include_lib("cowboy/include/cowboy.hrl").

%% Function to allocate resources
allocate_resources() ->
    %% Monitor CPU and memory usage
    monitor(),
    %% Check if automatic scaling is necessary
    %% and allocate the required resources
    ...
    ok.

%% Monitor CPU and memory usage
monitor() ->
    {ok, Pid} = os:cmd("top -n 1 -b | awk '/^%Cpu/{print $2}'"),
    CpuUsage = list_to_float(string:strip(Pid)),
    {ok, Pid2} = os:cmd("free | awk '/Mem/{print $3/$2}'"),
    MemoryUsage = list_to_float(string:strip(Pid2)),

    %% Update allocated resources
    NewResources = update_resources(CpuUsage, MemoryUsage),

    %% Check if automatic scaling is necessary
    %% and allocate the required resources
    ...
    ok.
