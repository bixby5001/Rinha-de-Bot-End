-module(pessoas_handler).
-export([
    handle_request/2,
    handle_request_region1/2,
    handle_request_region2/2,
    handle_request_region3/2
]).

-include_lib("cowboy/include/cowboy.hrl").
-include_lib("jsx/include/jsx.hrl").

%% Função para lidar com os diferentes tipos de requisição
handle_request(Req, State) ->
    {Method, Path} = cowboy_req:method(Req),
    case {Method, Path} of
        {'GET', <<"/pessoas">>} ->
            handle_get(Req, State);
        {'POST', <<"/pessoas">>} ->
            handle_create(Req, State);
        {'GET', [<<"pessoas">>, ID/binary]} ->
            handle_get_by_id(Req, State, binary_to_integer(ID));
        {'PUT', [<<"pessoas">>, ID/binary]} ->
            handle_update(Req, State, binary_to_integer(ID));
        {'DELETE', [<<"pessoas">>, ID/binary]} ->
            handle_delete(Req, State, binary_to_integer(ID));
        _ ->
            %% Rota não suportada
            cowboy_req:reply(404, #{}, <<"Route not found">>, Req, State)
    end.

%% Função para lidar com a criação de uma pessoa
handle_create(Req, State) ->
    {ok, Body, Req2} = cowboy_req:body(Req),
    case jsx:decode(Body) of
        {ok, Data} ->
            io:format("Criar pessoa: ~p~n", [Data]),
            {ok, Req2, State};
        {error, Reason} ->
            io:format("Erro na decodificação JSON: ~p~n", [Reason]),
            {bad_request, Req2, State}
    end.

%% Função para lidar com a obtenção de uma pessoa
handle_get(Req, State) ->
    %% Implementação da função handle_get para todas as regiões
    {ok, Req2, State}.

%% Função para lidar com a obtenção de uma pessoa por ID
handle_get_by_id(Req, State, ID) ->
    %% Implementação da função handle_get_by_id para todas as regiões
    {ok, Req2, State}.

%% Função para lidar com a atualização de uma pessoa
handle_update(Req, State, ID) ->
    {ok, Body, Req2} = cowboy_req:body(Req),
    case jsx:decode(Body) of
        {ok, Data} ->
            io:format("Atualizar pessoa ~p: ~p~n", [ID, Data]),
            {ok, Req2, State};
        {error, Reason} ->
            io:format("Erro na decodificação JSON: ~p~n", [Reason]),
            {bad_request, Req2, State}
    end.

%% Função para lidar com a exclusão de uma pessoa
handle_delete(Req, State, ID) ->
    io:format("Excluir pessoa ~p~n", [ID]),
    {ok, Req, State}.

%% Funções específicas para a Região 1
handle_request_region1(Req, State) ->
    {ok, Req2, State}.

%% Funções específicas para a Região 2
handle_request_region2(Req, State) ->
    {ok, Req2, State}.

%% Funções específicas para a Região 3
handle_request_region3(Req, State) ->
    {ok, Req2, State}.
