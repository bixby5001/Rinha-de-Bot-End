**Imports:**

> Importa os módulos Scala e Gatling necessários para a simulação.

**Classe de definição:**

> Cria a classe RinhaBackendSimulation que estende Simulation.

**Protocolo HTTP:**

> Define a URL base e o cabeçalho do agente do usuário.

**Cenários:**

> Define cenários para criar, consultar e pesquisar pessoas.

**Perfis de injeção:**

> Configura as taxas de injeção para diferentes cenários.

**onStart Override:**

> Executado antes do início da simulação.
    * Obtém informações de hardware usando uma solicitação GET para `/hardware`.
    * Extrai dados de hardware da sessão e cria um mapa.
    * Adiciona as informações de hardware ao cabeçalho de saída.

**Documentação:**

> **Imports:**

    * `scala.util.Random`: Fornece um gerador de números aleatórios.
    * `io.gatling.core.Predef`: Fornece definições predefinidas para cenários, protocolos e outros recursos.
    * `io.gatling.http.Predef`: Fornece definições predefinidas para solicitações HTTP.

> **Classe de definição:**

    * `Simulation`: Classe base para todas as simulações Gatling.

> **Protocolo HTTP:**

    * `baseUrl()`: Define a URL base para todas as solicitações HTTP.
    * `userAgentHeader()`: Define o cabeçalho do agente do usuário para todas as solicitações HTTP.

> **Cenários:**

    * `scenario()`: Define um cenário de simulação.

> **Perfis de injeção:**

    * `constantUsersPerSec()`: Define uma taxa de injeção constante de usuários por segundo.
    * `rampUsersPerSec()`: Define uma taxa de injeção crescente de usuários por segundo.

> **onStart Override:**

    * `super.onStart(event)`: Chama o método `onStart()` da classe base.
    * `exec()`: Executa uma solicitação ou um bloco de código antes do início da simulação.
    * `session()`: Obtém a sessão de simulação atual.
    * `as[Map[String, Any]]`: Converte a sessão em um mapa.
    * `setAll()`: Adiciona todas as chaves e valores do mapa à sessão.

**Adição de informações de hardware ao cabeçalho de saída:**

> A seção de informações de hardware no cabeçalho de saída contém os seguintes dados:

    * `CONTAINER ID`: O ID do contêiner que executou a solicitação.
    * `NAME`: O nome do contêiner que executou a solicitação.
    * `CPU%`: A porcentagem de uso da CPU do contêiner.
    * `CPU USAGE/LIMIT`: A taxa de uso da CPU do contêiner em relação ao limite.
    * `MEM%`: A porcentagem de uso da memória do contêiner.
    * `MEM USAGE/LIMIT`: A taxa de uso da memória do contêiner em relação ao limite.
    * `NET I/O`: A taxa de entrada/saída de rede do contêiner.
    * `BLOCK I/O`: A taxa de entrada/saída de disco do contêiner.
    * `PIDS`: O número de processos em execução no contêiner.
    * `GFLOPS/WATTS`: O desempenho de ponto flutuante por watt do contêiner.

> As informações de hardware são obtidas usando uma solicitação GET para o endpoint `/hardware`. Os dados são extraídos da sessão de simulação e colocados em um mapa. O mapa é então adicionado ao cabeçalho de saída usando o método `setAll()`.

Espero que isso ajude!