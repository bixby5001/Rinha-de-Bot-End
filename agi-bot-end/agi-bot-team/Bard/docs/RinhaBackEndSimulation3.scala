import scala.util.Random
import io.gatling.core.Predef._
import io.gatling.http.Predef._

class RinhaBackendSimulation extends Simulation {

  val httpProtocol = http
    .baseUrl("http://localhost:9999")
    .userAgentHeader("Agente do Caos - 2023")

  // Definições de cenários para criação, consulta e pesquisa de pessoas
  val criacaoEConsultaPessoas = scenario("Criação E Talvez Consulta de Pessoas")
    // ...
  val buscaPessoas = scenario("Busca Válida de Pessoas")
    // ...
  val buscaInvalidaPessoas = scenario("Busca Inválida de Pessoas")
    // ...

  // Perfis de injeção para diferentes cenários
  setUp(
    criacaoEConsultaPessoas.inject(
      // ...
    ),
    buscaPessoas.inject(
      // ...
    ),
    buscaInvalidaPessoas.inject(
      // ...
    )
  ).protocols(httpProtocol)

  // Sobrescreve o método onStart para obter informações de hardware antes de cada solicitação
  override def onStart(event: SimulationEvent) {
    super.onStart(event)

    // Executa uma solicitação para obter informações de hardware
    val hardwareInfo = exec(http("obter informações de hardware")
      .get("/hardware")
    )

    // Adiciona uma nova seção ao cabeçalho de saída para dados de hardware
    setUp(
      // ...
      hardwareInfo.exec {
        session =>
          val hardware = session("hardware").asInstanceOf[Map[String, Any]]
          val hardwareSection = Map(
            "CONTAINER_ID" -> hardware("CONTAINER_ID"),
            "NOME" -> hardware("CONTAINER_NAME"),
            "CPU%" -> hardware("CPU_USAGE_PERCENT"),
            "CPU_USAGE_LIMIT" -> hardware("CPU_USAGE_LIMIT"),
            "MEM%" -> hardware("MEM_USAGE_PERCENT"),
            "MEM_USAGE_LIMIT" -> hardware("MEM_USAGE_LIMIT"),
            "NET_IO" -> hardware("NET_IO"),
            "BLOCK_IO" -> hardware("BLOCK_IO"),
            "PIDS" -> hardware("PIDS"),
            "GFLOPS_PER_WATT" -> hardware("GFLOPS_PER_WATT")
          )
          session.setAll(hardwareSection)
      }
    )
  }
}
