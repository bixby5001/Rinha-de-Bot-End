import threading
import time
import psutil
import pandas as pd
import multiprocessing
import geocoder
import subprocess
import pyshorteners
import requests
from flask import Flask, jsonify, request

app = Flask(__name__)

class ExcitaTempAPI:
    def __init__(self, manufacturer, model, serial_number, thermal_zones):
        self.data = {}
        self.server_info = {
            "manufacturer": manufacturer,
            "model": model,
            "serial_number": serial_number,
            "thermal_zones": thermal_zones
        }
        self.snapshot_timer = threading.Timer(60, self._take_snapshot)
        self.snapshot_timer.start()

    def get_data(self):
        return self.data

    def _take_snapshot(self):
        self.data = self._get_data()

        current_memory_usage = psutil.virtual_memory().used / (1024 ** 2)
        if current_memory_usage > 200:
            self._clear_data()

        self.snapshot_timer = threading.Timer(60, self._take_snapshot)
        self.snapshot_timer.start()

    def _clear_data(self):
        self.data = {}

    def _get_temperatures(self):
        temperatures = {}
        for cpu in psutil.sensors_temperatures().keys():
            temperatures[cpu] = psutil.sensors_temperatures()[cpu][0].current
        return temperatures

    def _get_consumptions(self):
        processes = psutil.process_iter()
        df = pd.DataFrame()
        for process in processes:
            try:
                pid = process.pid
                memory_percent = process.memory_percent()
                if memory_percent > 0:
                    df = df.append({
                        "pid": pid,
                        "memory": process.memory_info().rss,
                        "cpu": process.cpu_percent(),
                        "frequency": process.cpu_freq().current,
                        "network": process.net_io_counters().bytes_sent + process.net_io_counters().bytes_recv,
                    }, ignore_index=True)
            except psutil.NoSuchProcess:
                pass
        return df.to_dict(orient='records')

    def _get_data(self):
        temperatures = self._get_temperatures()
        consumptions = self._get_consumptions()

        g = geocoder.ip('me')
        position = {
            "latitude": g.latlng[0],
            "longitude": g.latlng[1]
        }

        return {
            "temperatures": temperatures,
            "consumptions": consumptions,
            "position": position
        }

class ExcitaTempAPIWithIP(ExcitaTempAPI):
    def __init__(self, manufacturer, model, serial_number, thermal_zones):
        super().__init__(manufacturer, model, serial_number, thermal_zones)
        self.public_ip = self._get_public_ip()

    def _get_public_ip(self):
        response = requests.get('https://api.ipify.org?format=json')
        if response.status_code == 200:
            ip_data = response.json()
            public_ip = ip_data['ip']
            return public_ip
        else:
            return None

    def _generate_short_url(self, url):
        shortener = pyshorteners.Shortener()
        short_url = shortener.short(url)
        return short_url

    def publish_data(self):
        while True:
            data = self.get_data()
            url = f"http://{self.public_ip}:{self.port}"
            short_url = self._generate_short_url(url)
            print(f"Short URL: {short_url}")
            time.sleep(60)

    def start_flask(self):
        process = multiprocessing.Process(target=self._run_flask_app)
        process.start()
        return process

    def _run_flask_app(self):
        app.run(debug=True, port=self.port)

    def _get_data(self):
        data = super()._get_data()
        data["public_ip"] = self.public_ip
        return data

@app.route('/')
def index():
    return "Welcome to the ExcitaTemp API!"

@app.route('/data', methods=['GET'])
def get_data():
    data = api.get_data()
    return jsonify(data)

@app.route('/server-info', methods=['GET'])
def get_server_info():
    server_info = api.server_info
    return jsonify(server_info)

# Server configurations
manufacturer = "Server Manufacturer"
model = "Server Model"
serial_number = "Server Serial Number"
thermal_zones = {
    "cpu": 80,  # Nominal temperature of 80 °C for the CPU
    "gpu": 70,  # Nominal temperature of 70 °C for the GPU
}

api = ExcitaTempAPIWithIP(manufacturer, model, serial_number, thermal_zones)
api.start_flask()
api.publish_data()