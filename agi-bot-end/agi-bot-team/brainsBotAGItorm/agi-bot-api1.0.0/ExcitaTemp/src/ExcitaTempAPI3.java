// igual a ExcitaTempAPI3.java verificado por zeh sobrinho em 31 de dezembro de 2023

import java.util.Timer;
import java.util.TimerTask;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.commons.text.RandomStringGenerator;
import org.apache.commons.text.CharacterPredicates;

import oshi.SystemInfo;
import oshi.hardware.HardwareAbstractionLayer;
import oshi.hardware.CentralProcessor;
import oshi.hardware.CentralProcessor.TickType;
import oshi.hardware.GlobalMemory;
import oshi.software.os.OperatingSystem;
import oshi.software.os.OSProcess;
import oshi.util.FormatUtil;

public class ExcitaTempAPI {
    private Map<String, Object> data;
    private Map<String, Object> serverInfo;
    private Timer snapshotTimer;

    public ExcitaTempAPI(String fabricante, String modelo, String numeroSerie, Map<String, Integer> zonaTermicas) {
        this.data = new HashMap<>();
        this.serverInfo = new HashMap<>();
        this.serverInfo.put("fabricante", fabricante);
        this.serverInfo.put("modelo", modelo);
        this.serverInfo.put("numero_serie", numeroSerie);
        this.serverInfo.put("zona_termicas", zonaTermicas);
        this.snapshotTimer = new Timer();
        this.snapshotTimer.schedule(new SnapshotTask(), 0, 60000);
    }

    public Map<String, Object> getData() {
        return this.data;
    }

    private void takeSnapshot() {
        this.data = getDataFromSystem();
        long currentMemoryUsage = getMemoryUsage() / (1024 * 1024);
        if (currentMemoryUsage > 200) {
            clearData();
        }
    }

    private long getMemoryUsage() {
        SystemInfo systemInfo = new SystemInfo();
        GlobalMemory memory = systemInfo.getHardware().getMemory();
        return memory.getTotal() - memory.getAvailable();
    }

    private void clearData() {
        this.data.clear();
    }

    private Map<String, Object> getDataFromSystem() {
        SystemInfo systemInfo = new SystemInfo();
        HardwareAbstractionLayer hardware = systemInfo.getHardware();
        CentralProcessor processor = hardware.getProcessor();
        GlobalMemory memory = hardware.getMemory();
        OperatingSystem os = systemInfo.getOperatingSystem();

        Map<String, Object> temperatures = new HashMap<>();
        temperatures.put("cpu", processor.getTemperature());
        if (hardware.getSensors().getFans().length > 0) {
            temperatures.put("fan", hardware.getSensors().getFans()[0].getSpeed());
        }
        Map<String, Long> cpuLoadTicks = processor.getProcessorCpuLoadTicks();
        List<Long> cpuLoad = new ArrayList<>();
        for (TickType tick : TickType.values()) {
            cpuLoad.add(cpuLoadTicks.get(tick) / processor.getLogicalProcessorCount());
        }
        temperatures.put("load", cpuLoad);

        Map<String, Object> consumptions = new HashMap<>();
        List<OSProcess> processes = os.getProcesses(10, OperatingSystem.ProcessSort.CPU);
        for (OSProcess process : processes) {
            consumptions.put(process.getName(), process.getProcessCpuLoadBetweenTicks());
        }

        Map<String, Object> position = new HashMap<>();
        try {
            InetAddress ipAddress = InetAddress.getLocalHost();
            position.put("ip_address", ipAddress.getHostAddress());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        Map<String, Object> data = new HashMap<>();
        data.put("temperatures", temperatures);
        data.put("consumptions", consumptions);
        data.put("position", position);

        return data;
    }

    private static String getPublicIpAddress() throws IOException {
        URL url = new URL("https://api.ipify.org/");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String ipAddress = reader.readLine();
        reader.close();

        return ipAddress;
    }

    private class SnapshotTask extends TimerTask {
        @Override
        public void run() {
            takeSnapshot();
        }
    }

    public static void main(String[] args) {
        // Exemplo de uso
        String fabricante = "Fabricante do servidor";
        String modelo = "Modelo do servidor";
        String numeroSerie = "Número de série do servidor";
        Map<String, Integer> zonaTermicas = new HashMap<>();
        zonaTermicas.put("zona1", 50);
        zonaTermicas.put("zona2", 60);
        zonaTermicas.put("zona3", 70);

        ExcitaTempAPI api = new ExcitaTempAPI(fabricante, modelo, numeroSerie, zonaTermicas);
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    String ipAddress = getPublicIpAddress();
                    System.out.println("Public IP Address: " + ipAddress);
                    System.out.println("Data: " + api.getData());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, 0, 60000);
    }
}