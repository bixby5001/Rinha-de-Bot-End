import threading
import time
import psutil
import pandas as pd
import multiprocessing
import geocoder
import subprocess
import pyshorteners
import requests
from flask import Flask, jsonify, request

app = Flask(__name__)

class ExcitaTempAPI:
    def __init__(self, fabricante, modelo, numero_serie, zona_termicas):
        self.data = {}
        self.server_info = {
            "fabricante": fabricante,
            "modelo": modelo,
            "numero_serie": numero_serie,
            "zona_termicas": zona_termicas
        }
        self.snapshot_timer = threading.Timer(60, self._take_snapshot)
        self.snapshot_timer.start()

    def get_data(self):
        return self.data

    def _take_snapshot(self):
        self.data = self._get_data()

        current_memory_usage = psutil.virtual_memory().used / (1024 ** 2)
        if current_memory_usage > 200:
            self._clear_data()

        self.snapshot_timer = threading.Timer(60, self._take_snapshot)
        self.snapshot_timer.start()

    def _clear_data(self):
        self.data = {}

    def _get_temperatures(self):
        temperatures = {}
        for cpu in psutil.sensors_temperatures().keys():
            temperatures[cpu] = psutil.sensors_temperatures()[cpu][0].current
        return temperatures

    def _get_consumptions(self):
        processes = psutil.process_iter()
        df = pd.DataFrame()
        for process in processes:
            try:
                pid = process.pid
                memory_percent = process.memory_percent()
                if memory_percent > 0:
                    df = df.append({
                        "pid": pid,
                        "memory": process.memory_info().rss,
                        "cpu": process.cpu_percent(),
                        "frequency": process.cpu_freq().current,
                        "network": process.net_io_counters().bytes_sent + process.net_io_counters().bytes_recv,
                    }, ignore_index=True)
            except psutil.NoSuchProcess:
                pass
        return df.to_dict(orient='records')

    def _get_data(self):
        temperatures = self._get_temperatures()
        consumptions = self._get_consumptions()

        g = geocoder.ip('me')
        position = {
            "latitude": g.latlng[0],
            "longitude": g.latlng[1]
        }

        return {
            "temperatures": temperatures,
            "consumptions": consumptions,
            "position": position
        }

# Atualizando a classe ExcitaTempAPI
fabricante = "Fabricante do servidor"
modelo = "Modelo do servidor"
numero_serie = "Número de série do servidor"
zona_termicas = {
    "cpu": 80,  # Temperatura nominal de 80 °C para a CPU
    "gpu": 70,  # Temperatura nominal de 70 °C para a GPU
    "tpu": 60   # Temperatura nominal de 60 °C para a TPU
}
api = ExcitaTempAPI(fabricante, modelo, numero_serie, zona_termicas)

def run_flask_app():
    from flask import Flask
    app = Flask(__name__)
    # Definições da aplicação Flask
    app.run(debug=True, port=5000)

def start_flask():
    process = multiprocessing.Process(target=run_flask_app)
    process.start()
    return process

def generate_short_url(url):
    shortener = pyshorteners.Shortener()
    short_url = shortener.short(url)
    return short_url

def publish_data():
    ip_publico = '192.168.0.1'  # Substitua pelo IP público real
    porta = '8080'  # Substitua pela porta real

    while True:
        data = api.get_data()
        url = f"http://{ip_publico}:{porta}"
        short_url = generate_short_url(url)
        print(f"URL curta: {short_url}")
        time.sleep(60)

# Iniciando a publicação dos dados
publish_data()