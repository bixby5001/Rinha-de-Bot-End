import java.util.List;

public class ZoopCluster {
    private List<PrevaylerPessoa> prevaylerPessoas;
    private String monitoramentoDocker;

    // Construtor
    public ZoopCluster(List<PrevaylerPessoa> prevaylerPessoas, String monitoramentoDocker) {
        this.prevaylerPessoas = prevaylerPessoas;
        this.monitoramentoDocker = monitoramentoDocker;
    }

    // Métodos getters e setters
    public List<PrevaylerPessoa> getPrevaylerPessoas() {
        return prevaylerPessoas;
    }

    public void setPrevaylerPessoas(List<PrevaylerPessoa> prevaylerPessoas) {
        this.prevaylerPessoas = prevaylerPessoas;
    }

    public String getMonitoramentoDocker() {
        return monitoramentoDocker;
    }

    public void setMonitoramentoDocker(String monitoramentoDocker) {
        this.monitoramentoDocker = monitoramentoDocker;
    }
}

public class PrevaylerPessoa {
    private int requestsValidos;
    private int requestsInvalidos;
    private int requestsInsercao;
    private String monitoramentoDocker;

    // Construtor
    public PrevaylerPessoa(int requestsValidos, int requestsInvalidos, int requestsInsercao, String monitoramentoDocker) {
        this.requestsValidos = requestsValidos;
        this.requestsInvalidos = requestsInvalidos;
        this.requestsInsercao = requestsInsercao;
        this.monitoramentoDocker = monitoramentoDocker;
    }

    // Métodos getters e setters
    public int getRequestsValidos() {
        return requestsValidos;
    }

    public void setRequestsValidos(int requestsValidos) {
        this.requestsValidos = requestsValidos;
    }

    public int getRequestsInvalidos() {
        return requestsInvalidos;
    }

    public void setRequestsInvalidos(int requestsInvalidos) {
        this.requestsInvalidos = requestsInvalidos;
    }

    public int getRequestsInsercao() {
        return requestsInsercao;
    }

    public void setRequestsInsercao(int requestsInsercao) {
        this.requestsInsercao = requestsInsercao;
    }

    public String getMonitoramentoDocker() {
        return monitoramentoDocker;
    }

    public void setMonitoramentoDocker(String monitoramentoDocker) {
        this.monitoramentoDocker = monitoramentoDocker;
    }
}