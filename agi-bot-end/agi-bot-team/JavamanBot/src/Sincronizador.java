package sincronizador;

import docker.Monitor;

public class Sincronizador {
    public void autoEscalonamento(Monitor containerMonitor) {
        double cpuPercentage = containerMonitor.getCpuPercentage();
        double memoryPercentage = containerMonitor.getMemoryPercentage();

        if (cpuPercentage > 90.0 || memoryPercentage > 80.0) {
            // Lógica para criar uma nova instância da API no container
        }

        if (cpuPercentage < 80.0 && memoryPercentage < 90.0) {
            // Lógica para destruir uma instância da API no container
        }
    }
}

/*
zoopCluster.sincronizador = new Sincronizador();

// Verifique se é necessário escalonar na região A
zoopCluster.sincronizador.autoEscalonamento(zoopCluster.regiaoA.dockerMonitor);

// Verifique se é necessário escalonar na região B
zoopCluster.sincronizador.autoEscalonamento(zoopCluster.regiaoB.dockerMonitor);*/