import org.prevayler.Prevayler;
import org.prevayler.PrevaylerFactory;
import org.prevayler.Query;
import org.prevayler.Transaction;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class PessoaRepository {

    private Prevayler<List<Pessoa>> prevayler;

    public PessoaRepository() {
        try {
            prevayler = PrevaylerFactory.createPrevayler(new File("pessoa_directory"), new ArrayList<>());
        } catch (IOException | ClassNotFoundException e) {
            // Trate adequadamente as exceções
        }
    }

    public void addPessoa(Pessoa pessoa) {
        prevayler.execute(new AddPessoaTransaction(pessoa));
    }

    public List<Pessoa> searchPessoa(String fuzzyQuery) {
        return prevayler.execute(new SearchPessoaQuery(fuzzyQuery));
    }

    // Implemente outras operações que você precisa, como remover pessoa, atualizar pessoa, etc.

    private static class AddPessoaTransaction implements Transaction<List<Pessoa>> {
        private Pessoa pessoa;

        public AddPessoaTransaction(Pessoa pessoa) {
            this.pessoa = pessoa;
        }

        @Override
        public void executeOn(List<Pessoa> pessoas, Date executionTime) {
            pessoas.add(pessoa);
        }
    }

    private static class SearchPessoaQuery implements Query<List<Pessoa>, List<Pessoa>> {
        private String fuzzyQuery;

        public SearchPessoaQuery(String fuzzyQuery) {
            this.fuzzyQuery = fuzzyQuery;
        }

        @Override
        public List<Pessoa> query(List<Pessoa> pessoas, Date queryTime) {
            // Implemente a lógica de busca fuzzy aqui e retorne a lista de pessoas correspondentes
        }
    }
}