package prevayler;

class Pessoa {
    int validRequests;
    int invalidRequests;
    int insertionRequests;

    public Pessoa(int validRequests, int invalidRequests, int insertionRequests) {
        this.validRequests = validRequests;
        this.invalidRequests = invalidRequests;
        this.insertionRequests = insertionRequests;
    }
}

package docker;

class Monitor {
    String containerId;
    String name;
    double cpuPercentage;
    String cpuUsageLimit;
    String memoryUsageLimit;
    double memoryPercentage;
    String netIO;
    String blockIO;
    String lanGbps;
    String wanGbps;
    int pids;
}

package sincronizador;

// Defina aqui a classe correspondente ao Sincronizador e Auto Escalonamento de VMs

import docker.Monitor;

public class Sincronizador {
    public void autoEscalonamento(Monitor containerMonitor) {
        double cpuPercentage = containerMonitor.getCpuPercentage();
        double memoryPercentage = containerMonitor.getMemoryPercentage();

        if (cpuPercentage > 90.0 || memoryPercentage > 80.0) {
            // Lógica para criar uma nova instância da API no container
        }

        if (cpuPercentage < 80.0 && memoryPercentage < 90.0) {
            // Lógica para destruir uma instância da API no container
        }
    }
}

package zoop;

class Regiao {
    int validRequests;
    int invalidRequests;
    int insertionRequests;
    Monitor dockerMonitor;

    public Regiao(int validRequests, int invalidRequests, int insertionRequests) {
        this.validRequests = validRequests;
        this.invalidRequests = invalidRequests;
        this.insertionRequests = insertionRequests;
    }
}

class ZoopCluster {
    Pessoa prevaylerPessoa;
    Sincronizador sincronizador;
    Regiao regiaoA;
    Regiao regiaoB;
}

public class Main {
    public static void main(String[] args) {
        ZoopCluster zoopCluster = new ZoopCluster();

        zoopCluster.prevaylerPessoa = new Pessoa(100, 10, 20);
        zoopCluster.regiaoA = new Regiao(50, 5, 10);
        zoopCluster.regiaoB = new Regiao(60, 6, 12);

        System.out.println("Dados do ZoopCluster:");
        System.out.println("Prevayler Pessoa - Valid Requests: " + zoopCluster.prevaylerPessoa.validRequests);
        System.out.println("Prevayler Pessoa - Invalid Requests: " + zoopCluster.prevaylerPessoa.invalidRequests);
        System.out.println("Prevayler Pessoa - Insertion Requests: " + zoopCluster.prevaylerPessoa.insertionRequests);
        
        /*
        zoopCluster.prevaylerPessoa = new Pessoa(100, 10, 20);
        zoopCluster.regiaoA = new Regiao(50, 5, 10);
        zoopCluster.regiaoB = new Regiao(60, 6, 12);
        */