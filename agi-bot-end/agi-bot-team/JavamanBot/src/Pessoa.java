public class Pessoa {
    private int id;
    private String apelido;
    private String nome;
    private LocalDate nascimento;
    private String stack;

    public Pessoa(int id, String apelido, String nome, LocalDate nascimento, String stack) {
        this.id = id;
        this.apelido = apelido;
        this.nome = nome;
        this.nascimento = nascimento;
        this.stack = stack;
    }

    // getters e setters
    // ...
}