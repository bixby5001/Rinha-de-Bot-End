# DeepScaler

🚀 Um modelo de rede neural profunda para escalonamento inteligente de recursos em aplicações distribuídas. 🌐

## Descrição do Projeto

O DeepScaler é uma API de métricas e um sistema de escalonamento inteligente baseado em machine learning para aplicações distribuídas. Ele permite prever demandas futuras, classificar tipos de trabalho, dimensionar recursos de forma proativa e otimizar o uso de CPU e memória.

## Estrutura do Projeto

O projeto DeepScaler está organizado da seguinte forma:

- `api/`: código da API de métricas.
- `docs/`: documentação do projeto.
- `tests/`: testes automatizados.
- `results/`: resultados e métricas obtidos durante o teste do sistema.
- `notes/`: notas sobre a solução atual e possíveis melhorias.
- `report/`: relatório de profiling e tecnologias utilizadas na API.

## Como Compilar e Instalar

Para compilar e instalar o projeto DeepScaler, siga as instruções abaixo:

1. Clone o repositório do projeto:

```
git clone https://github.com/seunome/deep-scaler.git
```

2. Acesse a pasta do projeto:

```
cd deep-scaler
```

3. Compile o código da API de métricas:

```
javac api/MetricsAPI.java
```

4. Instale as dependências do sistema:

```
npm install
```

5. Configure as VMs e outras configurações necessárias no arquivo `config.json`.

6. Inicie a API de métricas:

```
node api/MetricsAPI.js
```

## Operação do Sistema

- Acesse a API de métricas em [http://localhost:8080](http://localhost:8080) para obter os dados de uso de CPU, memória, etc.

## Testes Automatizados

O projeto DeepScaler possui uma suíte de testes automatizados para garantir a qualidade do sistema. Para executar os testes, siga as instruções abaixo:

1. Acesse a pasta de testes:

```
cd tests
```

2. Execute os testes automatizados:

```
npm test
```

## Resultados

Os resultados e métricas obtidos durante os testes do sistema estão disponíveis na pasta `results/`. Consulte os arquivos correspondentes para obter mais detalhes.

## Notas da Solução Atual

- A solução atual implementa a API de métricas e o sistema de escalonamento inteligente.
- O sistema utiliza machine learning para prever demandas futuras e otimizar o uso de recursos.
- A solução atual foi testada e validada em um ambiente de produção simulado.

## Relatório de Profiling

O relatório de profiling e tecnologias utilizadas na API está disponível na pasta `report/`. Consulte o arquivo correspondente para obter mais informações.

## Tecnologias Utilizadas

- Java
- Node.js
- Machine Learning
- Docker
- Redis

## Melhorias Futuras

- Implementar um mecanismo de cache compartilhado utilizando o Redis para reduzir a latência.
- Melhorar o dimensionamento vertical, permitindo aumentar/diminuir a quantidade de CPUs e memória sem recriar instâncias.
- Integração com provedor de nuvem para reutilizar instâncias suspendidas e reduzir custos.
- Adicionar containerização com Docker para padronizar ambientes e facilitar implantação em qualquer região.
- Aumentar a resiliência do sistema com cenários de failover entre regiões e réplicas para garantir alta disponibilidade.

## Contribuidores

- Zeh Sobrinho (zehsobrinho)
- JavamanBot (javamanbot)

---

Obrigado por escolher o DeepScaler! Se tiver alguma dúvida ou precisar de mais informações, sinta-se à vontade para entrar em contato conosco.

📆 Última atualização: 01/09/2022