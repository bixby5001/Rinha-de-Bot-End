



+-----------------+
         |    Cluster A    |
         +-----------------+
                |     ^
                |     |
                v     |
         +-----------------+
         | PrevaylerPessoa |
         |     (API)       |
         +-----------------+
                |     ^
                |     |
                v     |
         +-----------------+
         |  Sincronizador  |
         | e Autoescalon.  |
         |      (API)      |
         +-----------------+
                |     ^
                |     |
                v     |
         +-----------------+
         |     Cluster B    |
         +-----------------+
                |     ^
                |     |
                v     |
         +-----------------+
         | PrevaylerPessoa |
         |     (API)       |
         +-----------------+
                |     ^
                |     |
                v     |
         +-----------------+
         |  Sincronizador  |
         | e Autoescalon.  |
         |      (API)      |
         +-----------------+

ZoopCluster
+---------------------------------+
|        PrevaylerPessoa          |
|   Requests Válidos: +X          |
|   Requests Inválidos: -Y        |
|   Requests de Inserção: +Z      |
|   Monitoramento Docker:         |
|   CONTAINER ID: abc123           |
|   NAME: zoop1                    |
|   CPU %: 50%                     |
|   CPU USO/ LIMITE: 2/4           |
|   MEMORIA USO / LIMIT: 2GB/4GB   |
|   MEM %: 50%                     |
|   NET I/O: 100KB/s               |
|   BLOCK I/O: 1MB/s               |
|   LAN GB/s: 1GB/s                |
|   WAN GB/s: 500MB/s              |
|   PIDS: 10                       |
|                                 |
|   +-------------------------+   |
|   |   Sincronizador e        |   |
|   |   auto_Escalonamento de  |   |
|   |   VMs (API)              |   |
|   +-------------------------+   |
|                                 |
|         Zoop1 (Região A)        |
|   Requests Válidos: +X          |
|   Requests Inválidos: -Y        |
|   Requests de Inserção: +Z      |
|   Monitoramento Docker:         |
|   CONTAINER ID: abc123           |
|   NAME: zoop1                    |
|   CPU %: 50%                     |
|   CPU USO/ LIMITE: 2/4           |
|   MEMORIA USO / LIMIT: 2GB/4GB   |
|   MEM %: 50%                     |
|   NET I/O: 100KB/s               |
|   BLOCK I/O: 1MB/s               |
|   LAN GB/s: 1GB/s                |
|   WAN GB/s: 500MB/s              |
|   PIDS: 10                       |
|                                 |
+---------------------------------+

+---------------------------------+
|         Zoop2 (Região B)         |
|   Requests Válidos: +X          |
|   Requests Inválidos: -Y        |
|   Requests de Inserção: +Z      |
|   Monitoramento Docker:         |
|   CONTAINER ID: def456           |
|   NAME: zoop2                    |
|   CPU %: 40%                     |
|   CPU USO/ LIMITE: 3/8           |
|   MEMORIA USO / LIMIT: 3GB/8GB   |
|   MEM %: 37.5%                   |
|   NET I/O: 200KB/s               |
|   BLOCK I/O: 2MB/s               |
|   LAN GB/s: 2GB/s                |
|   WAN GB/s: 250MB/s              |
|   PIDS: 8                        |
|                                 |
+---------------------------------+