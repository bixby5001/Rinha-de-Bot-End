#!/bin/bash

# Formato de saída JSON
JSON_OUTPUT="deepEnergy.json" 

# Coleta dados de hardware 
TIMESTAMP=$(date +"%Y-%m-%d %H:%M:%S")

# CPU
CPU_USAGE=$(top -bn1 | grep "Cpu(s)" | sed "s/.*, *\([0-9.]*\)%* id.*/\1/")
CPU_LIMIT="4" 
CORES=($(sensors | grep "Core 0" | awk '{print $2}') $(sensors | grep "Core 1" | awk '{print $2}') $(sensors | grep "Core 2" | awk '{print $2}') $(sensors | grep "Core 3" | awk '{print $2}'))

# Memória  
MEM_USAGE=$(free -m | awk 'NR==2 {print $3}')
MEM_LIMIT=$(free -m | awk 'NR==2 {print $2}')
MEM_USAGE_PCT=$(free -m | awk 'NR==2 {printf "%.2f%%\n", $3/$2*100}')

# Disco
DISK_READ=$(vmstat 1 2|tail -1|awk '{io=$10*512/1024} {print io}') 
DISK_WRITE=$(vmstat 1 2|tail -1|awk '{io=$11*512/1024} {print io}')

# Rede
NET_TX=$(ifconfig ens33 | grep "TX-Packets" | awk '{print $2}')
NET_RX=$(ifconfig ens33 | grep "RX-Packets" | awk '{print $2}')

# Sistema
SYSTEM_LOAD=$(uptime | awk -F, '{print $3}' | awk '{print $1}')
IRQS=$(vmstat 1 2|tail -1|awk '{io=$12} {print io}')

# Energia 
POWER_USAGE=$(sudo powertop --csv|grep "Power Usage"|awk '{print $3}')
POWER_GFLOPS=$(free -m|awk 'BEGIN {gb=1024*1024} NR==2 {printf "%.2f", $3/gb/1000}')

# Gera JSON
echo '{
  "timestamp":"'$TIMESTAMP'",
  "cpu":{
    "usage":"'$CPU_USAGE'",
    "limit":"'$CPU_LIMIT'",
    "cores":"['${CORES[0]}', '${CORES[1]}', '${CORES[2]}', '${CORES[3]}']"},
  "memory":{
    "usage":"'$MEM_USAGE'",
    "limit":"'$MEM_LIMIT'",
    "usage_pct":"'$MEM_USAGE_PCT'"},
  "network":{
    "tx":"'$NET_TX'",
    "rx":"'$NET_RX'"}, 
  "disk":{
    "read":"'$DISK_READ'",
    "write":"'$DISK_WRITE'"},
  "system":{
    "load":"'$SYSTEM_LOAD'",
    "irqs":"'$IRQS'"},
  "power":{
    "usage":"'$POWER_USAGE'",
    "gflops":"'$POWER_GFLOPS'"}
}'\
> $JSON_OUTPUT