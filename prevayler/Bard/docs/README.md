**Instalando Prevayler 2.6 no Eclipse**

Para instalar o Prevayler 2.6 no Eclipse, siga estas etapas:

1. Abra o Eclipse e selecione **File > New > Project**.
2. Selecione **Maven Project** e clique em **Next**.
3. No campo **Group ID**, digite **com.prevayler**.
4. No campo **Artifact ID**, digite **prevayler**.
5. No campo **Version**, digite **2.6**.
6. Clique em **Next**.
7. Selecione **Create a simple project (skip dependency management)** e clique em **Next**.
8. Clique em **Finish**.

O Eclipse criará um projeto Maven com o nome **prevayler**.

Para instalar as dependências do Prevayler, siga estas etapas:

1. Clique com o botão direito do mouse no projeto **prevayler** e selecione **Maven > Update Project**.
2. Clique em **OK** para iniciar o processo de atualização.

O Eclipse baixará e instalará as dependências do Prevayler.

**Iniciando o Prevayler**

Para iniciar o Prevayler, siga estas etapas:

1. Clique com o botão direito do mouse no projeto **prevayler** e selecione **Run As > Maven Build**.
2. Na caixa de diálogo **Maven Build**, insira o seguinte comando na caixa **Goals** e clique em **Run**.

```
clean install
```

O Eclipse compilará o projeto **prevayler** e instalará o Prevayler em seu computador.

**Criando uma API Prevayler**

Para criar uma API Prevayler, siga estas etapas:

1. Crie uma nova classe Java no projeto **prevayler**.
2. Declare a classe como uma subclasse de **com.prevayler.TransactionalRoot**.
3. Implemente os métodos **read()** e **write()**.

O método **read()** é usado para recuperar o estado da API do banco de dados Prevayler. O método **write()** é usado para salvar o estado da API no banco de dados Prevayler.

**Aqui está um exemplo de uma API Prevayler:**

```java
import com.prevayler.TransactionalRoot;

public class MyApi extends TransactionalRoot {

    private int counter;

    public MyApi() {
        counter = 0;
    }

    public int getCounter() {
        return counter;
    }

    public void incrementCounter() {
        counter++;
    }

    @Override
    public void read() {
        counter = (Integer) readObject("counter");
    }

    @Override
    public void write() {
        writeObject("counter", counter);
    }
}
```

**Testando a API Prevayler**

Para testar a API Prevayler, siga estas etapas:

1. Crie uma nova classe Java no projeto **prevayler**.
2. Adicione o seguinte código à classe.

```java
import com.prevayler.TransactionalRootFactory;

public class MyApiTest {

    public static void main(String[] args) throws Exception {
        MyApi api = TransactionalRootFactory.create("MyApi", MyApi.class);

        api.incrementCounter();
        System.out.println(api.getCounter()); // 1

        api.write();

        api = TransactionalRootFactory.create("MyApi", MyApi.class);

        System.out.println(api.getCounter()); // 1
    }
}
```

Este código cria uma instância da API **MyApi** e incrementa o contador. Em seguida, o código escreve o estado da API no banco de dados Prevayler. Por fim, o código cria uma nova instância da API **MyApi** e imprime o valor do contador.

Se você executar este código, ele imprimirá o seguinte:

```
1
1
```

**Conclusão**

A instalação do Prevayler 2.6 no Eclipse é um processo simples. Para instalar o Prevayler, siga as etapas descritas nesta seção.

--------------------------------

sobrinhosj@cs-610301933215-default:~$ jar -tf /root/.m2/repository/org/prevayler/prevayler-core/2.6/prevayler-core-2.6.jar
META-INF/
META-INF/MANIFEST.MF
org/
org/prevayler/
org/prevayler/foundation/
org/prevayler/foundation/monitor/
org/prevayler/foundation/serialization/
org/prevayler/implementation/
org/prevayler/implementation/clock/
org/prevayler/implementation/journal/
org/prevayler/implementation/publishing/
org/prevayler/implementation/snapshot/
org/prevayler/Clock.class
org/prevayler/foundation/Chunk.class
org/prevayler/foundation/Chunking.class
org/prevayler/foundation/Cool.class
org/prevayler/foundation/DeepCopier$Receiver.class
org/prevayler/foundation/DeepCopier.class
org/prevayler/foundation/DurableInputStream.class
org/prevayler/foundation/DurableOutputStream.class
org/prevayler/foundation/FileManager.class
org/prevayler/foundation/Guided.class
org/prevayler/foundation/monitor/LoggingMonitor.class
org/prevayler/foundation/monitor/Monitor.class
org/prevayler/foundation/monitor/NullMonitor.class
org/prevayler/foundation/monitor/SimpleMonitor.class
org/prevayler/foundation/ObjectInputStreamWithClassLoader.class
org/prevayler/foundation/serialization/DESSerializer$1.class
org/prevayler/foundation/serialization/DESSerializer.class
org/prevayler/foundation/serialization/GZIPSerializer.class
org/prevayler/foundation/serialization/JavaSerializer.class
org/prevayler/foundation/serialization/Serializer.class
org/prevayler/foundation/StopWatch.class
org/prevayler/foundation/Turn.class
org/prevayler/implementation/Capsule.class
org/prevayler/implementation/clock/BrokenClock.class
org/prevayler/implementation/clock/MachineClock.class
org/prevayler/implementation/clock/PausableClock.class
org/prevayler/implementation/journal/Journal.class
org/prevayler/implementation/journal/PersistentJournal.class
org/prevayler/implementation/journal/TransientJournal.class
org/prevayler/implementation/PrevalentSystemGuard.class
org/prevayler/implementation/PrevaylerDirectory$1.class
org/prevayler/implementation/PrevaylerDirectory$2.class
org/prevayler/implementation/PrevaylerDirectory.class
org/prevayler/implementation/PrevaylerImpl.class
org/prevayler/implementation/publishing/AbstractPublisher.class
org/prevayler/implementation/publishing/CentralPublisher.class
org/prevayler/implementation/publishing/POBox.class
org/prevayler/implementation/publishing/TransactionPublisher.class
org/prevayler/implementation/publishing/TransactionSubscriber.class
org/prevayler/implementation/snapshot/GenericSnapshotManager.class
org/prevayler/implementation/snapshot/NullSnapshotManager.class
org/prevayler/implementation/TransactionCapsule.class
org/prevayler/implementation/TransactionGuide.class
org/prevayler/implementation/TransactionTimestamp.class
org/prevayler/implementation/TransactionWithQueryCapsule.class
org/prevayler/Prevayler.class
org/prevayler/Query.class
org/prevayler/SureTransactionWithQuery.class
org/prevayler/Transaction.class
org/prevayler/TransactionWithQuery.class
META-INF/maven/
META-INF/maven/org.prevayler/
META-INF/maven/org.prevayler/prevayler-core/
META-INF/maven/org.prevayler/prevayler-core/pom.xml
META-INF/maven/org.prevayler/prevayler-core/pom.properties
sobrinhosj@cs-610301933215-default:~$ 

-----------------------------------------------